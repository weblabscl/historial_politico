<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('politicians@index');
});*/

Route::get('/', 'PoliticianController@index');
Route::get('/feeds/', 'PoliticianController@feeds');

Route::get('event/approved/{id}', 'EventController@approved');
Route::get('event/tobeapproved/{id}', 'EventController@tobeapproved');
Route::get('event/cancel/{id}', 'EventController@cancel');
Route::get('event/share/', 'EventController@share');

Route::resource('event', 'EventController');

//se debe colocar el get antes que el resource
Route::get('politician/showList', 'PoliticianController@showList');
Route::get('politician/profile/{name}', 'PoliticianController@profile');
Route::resource('politician', 'PoliticianController');

Route::get('/graphics', 'QuestionController@graphics');

//Encuesta
Route::get('/poll', 'QuestionController@poll');
Route::get('/question/selectPolitician', 'QuestionController@selectPolitician');
Route::get('/question/getPolitician', 'QuestionController@getPolitician');
Route::get('/question/editSurvey', 'QuestionController@editSurvey');

Route::resource('question', 'QuestionController');

Route::get('/answer/saveAnswer', 'AnswerController@saveAnswer');
Route::get('/answer/infoPolitician', 'AnswerController@infoPolitician');
Route::get('/answer/response/{id}/{hash}', 'AnswerController@response');
Route::resource('answer', 'AnswerController');

//Privilegios Usuarios
Route::get('/admin/listUsers', 'AdminController@listUsers');
Route::get('/admin/be/{id}/{type}', 'AdminController@be');

Route::get('instances/approved/{id}', 'InstanceController@approved');
Route::get('instances/tobeapproved/{id}', 'InstanceController@tobeapproved');
Route::get('instances/registries/{id}', 'InstanceController@registries');
Route::get('instances/addregistry/{id}', 'InstanceController@addregistry');
Route::resource('instances', 'InstanceController');

Auth::routes();
