<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['namespace' => 'api'], function () {
    Route::post('/login', 'UserController@login');
    Route::get('/details', 'UserController@details');
    Route::get('/politicians', 'UserController@politicians');
    Route::get('/cases', 'UserController@cases');
    Route::get('/eventLike/{id}/{delete}', 'UserController@eventLike');
    Route::get('/eventDislike/{id}/{delete}', 'UserController@eventDislike');
    Route::get('/eventCancel/{id}/{type}', 'UserController@eventCancel');
    Route::get('/registerToken/{token}/{device}', 'UserController@registerToken');
    Route::get('/data', 'UserController@data');
    

});
