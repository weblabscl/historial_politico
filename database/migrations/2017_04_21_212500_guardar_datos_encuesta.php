<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GuardarDatosEncuesta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('question_id')->index();
            $table->boolean('value')->default(0);
            $table->string('ip');
            $table->timestamps();
        });

        

        Schema::table('polls', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->foreign('question_id', 'answers_polls_id')
            ->references('id')
            ->on('questions')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        /*Schema::table('polls', function(Blueprint $table)
        {
            $table->dropForeign('answers_polls_id');
        });*/

        Schema::drop('polls');

    }
}
