<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PoliticianFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('politicians', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->foreign('university_id', 'politicians_universities_id')
                ->references('id')
                ->on('universities')
                ->onDelete('cascade');

                $table->foreign('city_id', 'politicians_cities_id')
                ->references('id')
                ->on('cities')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('politicians', function(Blueprint $table)
        {
            $table->dropForeign('politicians_universities_id');
            $table->dropForeign('politicians_cities_id');
        });
    }
}
