<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Politicians extends Migration
{
    public function up() {
        Schema::create('politicians', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->unsignedInteger('city_id')->index();
            $table->string('school');
            $table->string('politicalparty');
             $table->unsignedInteger('university_id')->index();
            $table->string('profession');

        });
    }

    public function down() {
        Schema::drop('politicians');
    }
}


