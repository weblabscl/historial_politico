<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliticianRegistryTable extends Migration
{
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::create('politician_registry', function(Blueprint $table)
		  {
		      $table->integer('politician_id')->unsigned()->nullable();
		      $table->foreign('politician_id')->references('id')
		            ->on('politicians')->onDelete('cascade');

		      $table->integer('registry_id')->unsigned()->nullable();
		      $table->foreign('registry_id')->references('id')
		            ->on('registries')->onDelete('cascade');

		      $table->timestamps();
		  });
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
    	    Schema::dropIfExists('politician_registry');
	}
}
