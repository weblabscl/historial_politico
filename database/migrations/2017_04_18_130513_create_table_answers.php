<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAnswers extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		
		  Schema::create('answers', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('question_id')->index();
			$table->unsignedInteger('politician_id')->index();
			$table->boolean('value')->default(0);
			$table->timestamps();
	 	  });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('answers');
	}
}
