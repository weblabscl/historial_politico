<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSurveys extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		
		 Schema::create('surveys', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('politician_id')->index();
		 	$table->string('ip');
		 	$table->longText('observation');
		 	$table->integer('value');
			$table->timestamps();
	 	  });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('surveys');
	}
}
