<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkAnswers extends Migration
{
	public function up()
	{
		Schema::table('answers', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->foreign('question_id', 'answers_questions_id')
				->references('id')
				->on('questions')
				->onDelete('cascade');

				$table->foreign('politician_id', 'answers_politicians_id')
				->references('id')
				->on('politicians')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('answers', function(Blueprint $table)
		{
			$table->dropForeign('answers_questions_id');
			$table->dropForeign('answers_politicians_id');
		});
	}
}
