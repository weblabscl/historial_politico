<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instance extends Model
{
	protected $table = 'instances';

	protected $primaryKey = 'id';

	protected $fillable = [
		'title',
		'description',
		'photo',
		'year',
	];

	public function politicians()
	{
	    return $this->belongsToMany('App\Politician')->withTimestamps();
	}

	 public function registries()
	    {
	        return $this->hasMany('App\Registry');
	    }
}
