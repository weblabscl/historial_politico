<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
	protected $table = 'answers';

	protected $primaryKey = 'id';

	protected $fillable = [
		'question_id',
		'politician_id',
		'value',
		'description'
	];

	public function like()
	{
		return $this->hasMany(
			'App\Models\Like',
			'answer_id',
			'answer_id'
		);
	}
	public function dislike()
	{
		return $this->hasMany(
			'App\Models\Dislike',
			'answer_id',
			'answer_id'
		);
	}

	public function politician()
	{
		return $this->belongsTo('App\Politician');
	}


	public function question()
	{
		return $this->belongsTo('App\Question');
	}
}
