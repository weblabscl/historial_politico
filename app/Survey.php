<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{

	protected $fillable = ['politician_id','ip','observation','value'];


	public function politician()
	{
		return $this->belongsTo('App\Politician');
	}
}
