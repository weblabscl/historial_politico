<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

    protected $primaryKey = 'id';

    protected $fillable = [
        'provinces_id',
        'name'
    ];

    public function province()
    {
        return $this->belongsTo('App\Province');
    }

    public function politicians()
    {
        return $this->hasMany('App\Politician');
    }
}

