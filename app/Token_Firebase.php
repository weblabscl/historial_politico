<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token_Firebase extends Model
{
    protected $table = 'token_firebase';

    protected $primaryKey = 'id';

    protected $fillable = [
        'device',
        'token',
        'created_at',
        'updated_at'
    ];
   
}
