<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Politician extends Model
{
    protected $table = 'politicians';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'city_id',
        'school',
        'university_id',
        'profession',
        'politicalparty'
    ];

    public function setNameAttribute($value)
{
    $this->attributes['name'] = $value;
    $this->attributes['slug'] = str_slug($value);
}

    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function university()
    {
        return $this->belongsTo('App\University');
    }

    public function registries()
	{
	    return $this->belongsToMany('App\Registry')->withTimestamps();
	}
}
