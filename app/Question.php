<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
	protected $table = 'questions';

	protected $primaryKey = 'id';

	protected $fillable = [
		'description',
		'type',
		'icon'
	];

	public function answer()
	{
		return $this->hasMany(
			'App\Models\Answer',
			'question_id',
			'question_id'
		);
	}
}
