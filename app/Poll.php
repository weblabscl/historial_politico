<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
	protected $table = 'polls';

	protected $primaryKey = 'id';

	protected $fillable = ['question_id','value','ip'];

}
