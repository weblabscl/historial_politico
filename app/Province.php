<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';

    protected $primaryKey = 'provinces_id';

    protected $fillable = [
        'regions_id',
        'provinces_name'
    ];

    public function region()
    {
        $this->belongsTo(
            'App\Models\Region',
            'regions_id',
            'regions_id'
        );
    }

    public function community()
    {
        return $this->hasMany(
            'App\Models\Communities',
            'provinces_id',
            'provinces_id'
        );
    }
}
