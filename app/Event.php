<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    protected $primaryKey = 'id';

    protected $fillable = [
        'description',
        'politician_id',
        'date',
        'link',
        'quote',
        'created_at',
        'updated_at'
    ];

    public function politician()
    {
        return $this->belongsTo('App\Politician');
    }
   
}
