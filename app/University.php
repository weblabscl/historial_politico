<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{

	protected $table = 'universities';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name'
    ];


    public function politicians()
    {
        return $this->hasMany('App\Politician');
    }
}
