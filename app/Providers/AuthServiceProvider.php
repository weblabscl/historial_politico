<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        $now = new \DateTime('now', new \DateTimeZone('UTC')); 

        Passport::tokensExpireIn($now->add(new \DateInterval('P1D'))); //1 day

        Passport::refreshTokensExpireIn($now->add(new \DateInterval('P30D'))); //30 days
        //
    }
}
