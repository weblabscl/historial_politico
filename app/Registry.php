<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registry extends Model
{
 	protected $table = 'registries';

	protected $primaryKey = 'id';

	protected $fillable = [
		'title',
		'description',
		'link',
		'state',
	];

	public function instance()
	{
		return $this->belongsTo('App\Instance');
	}

	public function politicians()
	{
	    return $this->belongsToMany('App\Politician')->withTimestamps();
	}

	/*$tl = Todolist::find(1);

	$category = new Category(['name' => 'Vacation']);

	$tl->categories()->save($category);*/
}

