<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'regions';

    protected $primaryKey = 'regions_id';

    protected $fillable = [
        'regions_name'
    ];

    public function province()
    {
        return $this->hasMany(
            'App\Models\Provinces',
            'regions_id',
            'regions_id'
        );
    }
}
