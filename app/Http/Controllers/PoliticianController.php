<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Politician;
use App\University;
use App\City;
use App\Event;
use Session;
use willvincent\Feeds\Facades\FeedsFacade;
use Illuminate\Support\Str;

class PoliticianController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	//public $layout = 'layouts.master'));

	public function __construct()
    {
         $this->middleware('auth', ['except' => ['index','profile']]);
    }

	
	public function index()
	{

		$politicians = Politician::orderBy('name')->where('type',1)->get();


		return view('politicians.index', compact('politicians'));
	}



	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$cities = City::orderBy('name','asc')->pluck('name', 'id');
		$universities = University::orderBy('name','asc')->pluck('name', 'id');
		return view('politicians.create',compact('universities','cities'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$politician = Politician::create([
			'name' => $request->input('name'),
			'city_id' => $request->input('city_id'),
			'school' => $request->input('school'),
			'politicalparty' => $request->input('politicalparty'),
			'university_id' => $request->input('university_id'),
			'profession' => $request->input('profession')
		]);

		//Session::flash('message', 'My message');

		if($politician){
			return redirect('/politician/showList')->with('success','Candidato agregado correctamente');
		}else{
			return redirect('/politician/showList')->with('alert','Error no se pudo agregar el candidato');
		}

	}



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function profile($name)
	{
		/*$politicians = Politician::All();
		foreach ($politicians as $key => $p) {
			$politician = Politician::find($p->id);
			if($politician) {
			    $politician->slug = str_slug($politician->name);
			    $politician->save();
			}
		}*/

        $politician = Politician::where('slug',$name)->orderBy('name')->with(['city','events' => function($q){
            $q->orderBy('date','DESC')->where('state',1);
        }])->where('type',1)->first();

		return View('politicians.profile',compact('politician'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$cities = City::pluck('name', 'id');
		$universities = University::pluck('name', 'id');

		  $politician = Politician::find($id);
		return view('politicians.edit',compact('universities','cities','politician'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$politician = Politician::find($id);
	      	$politician->fill($request->all());
	      	$politician->save();

	      	//Session::flash('message','Usuario editado correctamente');
	      
	      	if($politician){
			return redirect('/politician/showList')->with('success','Candidato editado correctamente');
		}else{
			return redirect('/politician/showList')->with('alert','Error no se pudo editar el candidato');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	public function showList()
	{	
		
		$politicians = Politician::orderBy('name')->with(['city','events' => function($q){
			$q->orderBy('date','DESC')->where('state',1);
		}])->where('type',1)->get();

		return view('politicians.showList', compact('politicians'));
	}

	public function feeds(){

		$names = array();
	    $ids = array();

	   $politicians = Politician::orderBy('name')->where('type',1)->get();
	   foreach ($politicians as $politician) {
	    	array_push($names, $politician->name);
	    	array_push($ids, $politician->id);
	    }

	    $i = 0;

		
	    #####################
	    ## RSS COOPERATIVA ##
	    #####################
		$feed = file_get_contents('https://www.cooperativa.cl/noticias/site/tax/port/all/rss_3___1.xml');
		$items = new \SimpleXMLElement($feed);

		foreach ($items->channel->item as  $item) {
		
	    	foreach ($names as $key => $name) {	
	    		if(strpos($item->description, $name) !== FALSE || strpos($item->title, $name) !== FALSE ){

	    			if( !Event::where('link',$item->link)->where('politician_id',$ids[$key])->first() ){
						$i++;

						$date = new \DateTime($item->pubDate);
						Event::create([
							'description' => $item->title,
							'politician_id' => $ids[$key],
							'link' => $item->link,
							'date' => $date->format('Y-m-d')
						]);
	    			}
	    			
	    		}
	    	}
	    }

	    #############
	    ## RSS AND ##
	    #############
		$feed = file_get_contents('http://www.adnradio.cl/feed.aspx?id=INICIO');
		$items = new \SimpleXMLElement($feed);

		foreach ($items->channel->item as  $item) {
		
	    	foreach ($names as $key => $name) {	
	    		if(strpos($item->description, $name) !== FALSE || strpos($item->title, $name) !== FALSE ){

	    			if( !Event::where('link',$item->link)->where('politician_id',$ids[$key])->first() ){
						$i++;

						$date = new \DateTime($item->pubDate);
						Event::create([
							'description' => $item->title,
							'politician_id' => $ids[$key],
							'link' => $item->link,
							'date' => $date->format('Y-m-d')
						]);
	    			}
	    			
	    		}
	    	}
	    }

		###################
	    ## RSS SOY CHILE ##
	    ###################
   		$feed = FeedsFacade::make('http://feeds.feedburner.com/soychilecl-politica?format=xml');
   		$data = array(
	      'title'     => $feed->get_title(),
	      'permalink' => $feed->get_permalink(),
	      'items'     => $feed->get_items(),
	    );

	    $items = $feed->get_items();

	    foreach ($items as  $item) {
		
	    	foreach ($names as $key => $name) {	
	    		if(strpos($item->get_description(), $name) !== FALSE || strpos($item->get_title(), $name) !== FALSE ){

	    			if( !Event::where('link',$item->get_permalink())->where('politician_id',$ids[$key])->first() ){
						$i++;

						$date = new \DateTime();
						Event::create([
							'description' => $item->get_title(),
							'politician_id' => $ids[$key],
							'link' => $item->get_permalink(),
							'date' => $date->format('Y-m-d')
						]);
	    			}
	    			
	    		}
	    	}
	    }

	    ################################
	    ## RSS Voz Ciudadana Noticias ##
	    ################################
   		$feed = FeedsFacade::make('http://feeds.feedburner.com/vozciudadananoticias/VCN?format=xml');
   		$data = array(
	      'title'     => $feed->get_title(),
	      'permalink' => $feed->get_permalink(),
	      'items'     => $feed->get_items(),
	    );

	    $items = $feed->get_items();

	    foreach ($items as  $item) {
		
	    	foreach ($names as $key => $name) {	
	    		if(strpos($item->get_description(), $name) !== FALSE || strpos($item->get_title(), $name) !== FALSE ){

	    			if( !Event::where('link',$item->get_permalink())->where('politician_id',$ids[$key])->first() ){
						$i++;

						$date = new \DateTime();
						Event::create([
							'description' => $item->get_title(),
							'politician_id' => $ids[$key],
							'link' => $item->get_permalink(),
							'date' => $date->format('Y-m-d')
						]);
	    			}
	    			
	    		}
	    	}
	    }


	    echo("Se registraron ".$i." eventos");
	}
}
