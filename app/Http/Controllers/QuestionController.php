<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Answer;
use App\Poll;
use App\Survey;

class QuestionController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$questions = Question::All();
		return view('questions.index',compact('questions'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return View('questions.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$question = Question::create([
			'description' => $request->input('description'),
			'type' => $request->input('type'),
			'icon' => $request->input('icon')
		]);

		//Session::flash('message', 'My message');

		if($question){
			return redirect('/question')->with('success','Item agregado correctamente');
		}else{
			return redirect('/question')->with('alert','Error no se pudo agregar el Item');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		  $question = Question::find($id);
		return view('questions.edit',compact('question'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$question = Question::find($id);
      	$question->fill($request->all());
      	$question->save();

      	//Session::flash('message','Usuario editado correctamente');
      
      	if($question){
			return redirect('/question')->with('success','Item editado correctamente');
		}else{
			return redirect('/question')->with('alert','Error no se pudo editar el Item');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	public function poll()
	{	
		$questions = Question::All();
		return view('questions.poll',compact('questions'));
	}

	public function selectPolitician(Request $request){
	
		$token = $request->session()->get('_token');
		$res = 0;
		if (!Survey::where('ip', $token)->first()) {
		
			Survey::create([
				'politician_id'  => $request->candidato,
				'observation'  => '',
				'value'  => 1,
				'ip' => $token
			]);

			$res = 1;
		}

		return json_encode($res);
	}

	public function editSurvey(Request $request){

		if($request->id != ''){

			$survey = Survey::find($request->id);

			$survey->value = 2;
			$survey->observation = $request->observation;

			$survey->save();
		
		}
	
		exit(json_encode(true));
	}

	public function getPolitician(Request $request){

		$result = array();		

		$questions = Question::All();

		$token = $request->session()->get('_token');

		$answers = Answer::with(['politician','question'])->get();

		# Guarda Respuesta del usuario
		if (!Poll::where('ip', $token)->first()) {
		
			if(!empty($questions)){
				foreach ($questions as $question) {
					
					$userAnswer = $request->input('question_'.$question->id);

					Poll::create([
						'question_id'  => $question->id,
						'value' => $userAnswer,
						'ip' => $token
					]);
				}
			}
		}


		#Busca Coincidencias
		if(!empty($answers)){
			foreach ($answers as $answer) {

				$userAnswer = $request->input('question_'.$answer->Question->id);

				$candidato = $answer->Politician->id;
				$candidatoNombre = $answer->Politician->name;
				$candidatoFoto = $answer->Politician->photo;

				if(!isset($result[$candidato])){
					$cantidad = 0;
					$porcentaje = 0;
					$result[$candidato] = array(
						"Id" => $candidato,
						"Porcentaje" => $porcentaje,
						"Cantidad" => $cantidad,
						"Candidato" => $candidatoNombre,
						"Foto" => $candidatoFoto,
						"Respuestas" => array()
					);
				}

				$coincide = ((int)$answer->value == (int)$userAnswer);

				$cantidad = $result[$candidato]["Cantidad"];
				
				if($coincide){
					$cantidad++;
				}

				$porcentaje = (int)round( ( $cantidad / sizeof($questions) ) * 100 );
				$result[$candidato]["Porcentaje"] = $porcentaje;
				$result[$candidato]["Cantidad"] = $cantidad;

				array_push($result[$candidato]["Respuestas"], array("Coincide" => $coincide,"Contenido" => $answer->Question->description));
			}
		}

		#Ordena por porcentaje de coincidencias
		usort($result, array($this,"sortByPercent"));



		if(!empty($result)){

			$max = $result[0]["Cantidad"];
			$result[0]["Ganador"] = true;
			$ganadores = 1;

			foreach ($result as $key => $candidato) {

				if($key){
					if($max == $candidato["Cantidad"]){
						$result[$key]["Ganador"] = true;	
						$ganadores++;
					}
				}
				
			}


			if($ganadores == 1){
				if (!Survey::where('ip', $token)->first()) {
					$survey = Survey::create([
						'politician_id'  => $result[0]["Id"],
						'observation'  => '',
						'value'  => 1,
						'ip' => $token
					]);

					$result[0]["insert"] = $survey->id;	
				}else{
					$result[0]["insert"] = null;	
				}
			}
		}

		$result = array("Candidatos" => $result,"Ganadores" => $ganadores);

		return json_encode($result);

	}

	static function sortByPercent($a,$b) {
       
       if ($a["Porcentaje"] == $b["Porcentaje"]) {
	        return 0;
	    }

	    return ($a["Porcentaje"] > $b["Porcentaje"]) ? -1 : 1;
	}

	
}
