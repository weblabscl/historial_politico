<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Response;
use App\Politician;
use App\University;
use App\City;
use App\Event;
use App\Instance;
use App\Token_Firebase;
use willvincent\Feeds\Facades\FeedsFacade;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function __construct(){
        $this->content = array();
    }

    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
    	    $user = Auth::user();
        	$this->content['token'] =  $user->createToken('Historia Politica')->accessToken;
        	$status = 200;
    	} else {
        	$this->content['error'] = "Unauthorised";
        	$status = 401;
    	}
    	return response()->json([$this->content, $status]);    
    }

    public function details(){

        $name = $_GET['name'];
        $politician = Politician::where('slug',$name)->orderBy('name')->with(['city','events' => function($q){
            $q->orderBy('date','DESC')->where('state',1);
        }])->get();

        return response()->json($politician);
    }

    //trae toda la wea
    public function data(){

        $politicians = Politician::orderBy('name')->with(['city','events' => function($q){
            $q->orderBy('date','DESC')->where('state',1);
        }])->where('type',1)->get();

        if($politicians){
            foreach ($politicians as $key => $politician) {
                if(isset($politician["photo"])){
                    $politicians[$key]["photo"] = asset('img/candidatos/'.$politician["photo"]);
                }

                foreach ($politician->events as $eventKey => $event) {
                    $politicians[$key]->events[$eventKey]["source"] = parse_url($event->link)["host"];
                }
            }
        }

        $cases = Instance::All();

        if($cases){
            foreach ($cases as $key => $case) {
                if(isset($case["photo"])){
                    $cases[$key]["photo"] = asset('img/casos/'.$case["photo"]);
                }
            }
        }

        $data = array("politicians" => $politicians,"cases" => $cases);

        return response()->json($data);
    }

    public function politicians(){

        $politicians = Politician::orderBy('name')->with(['city','events' => function($q){
            $q->orderBy('date','DESC')->where('state',1);
        }])->where('type',1)->get();

        if($politicians){
            foreach ($politicians as $key => $politician) {
                if(isset($politician["photo"])){
                    $politicians[$key]["photo"] = asset('img/candidatos/'.$politician["photo"]);
                }

                foreach ($politician->events as $eventKey => $event) {
                    $politicians[$key]->events[$eventKey]["source"] = parse_url($event->link)["host"];
                }
            }
        }

        return response()->json($politicians);
    }

     public function cases(){

        $cases = Instance::All();

        if($cases){
            foreach ($cases as $key => $case) {
                if(isset($case["photo"])){
                    $cases[$key]["photo"] = asset('img/casos/'.$case["photo"]);
                }
            }
        }

        return response()->json($cases);
    }

    public function eventLike($id,$delete){

        $event = Event::find($id);
        $event->like++;
        if($delete && $event->dislike > 0):
            $event->dislike--;
        endif;
        $event->save();

        return response()->json($event);
    }

    public function eventDislike($id,$delete){

        $event = Event::find($id);
        $event->dislike++;
        if($delete && $event->like > 0):
            $event->like--;
        endif;
        $event->save();

        return response()->json($event);
    }

    public function eventCancel($id,$type){

        $event = Event::find($id);
	if($type == 1):
		$event->like--;
	else:
		$event->dislike--;	
	endif;
        $event->save();

        return response()->json($event);
    }

    public function registerToken($token,$device){

        $client = Token_Firebase::create([
            'device' => $device,
            'token' => $token
        ]);

        return response()->json($client);

    }


}
