<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Politician;
use App\Token_Firebase;
use Thujohn\Twitter\Twitter;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class EventController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		
		$events_pendient = Event::where('state', 0)->get();
		$events = Event::where('state', 1)->get();
		return view('events.index',compact('events_pendient','events'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$politicians = Politician::orderBy('name','asc')->pluck('name','id');
		return view('events.create',compact('politicians'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//dd($request);
		$date = date_create_from_format('d/m/Y', $request->input('date'));
		$date = date_format($date,'Y-m-d');

		$quote = $request->input('quote')?1:0;

		$event = Event::create([
			'politician_id' => $request->input('politician_id'),
			'date' => $date,
			'description' => $request->input('description'),
			'quote' => $quote,
			'link' => $request->input('link')
		]);

		if(!empty($event)){
			return redirect('/event/create')->with('success','Gracias por colaborar, nuestro equipo revisará la información para verificar su veracidad.');
		}else{
			return redirect('/event/create')->with('alert','Error no se pudo agregar la información');
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$politicians = Politician::orderBy('name','asc')->pluck('name', 'id');

		  $event = Event::find($id);
		return view('events.edit',compact('politicians','event'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$event = Event::find($id);
		$date = date_create_from_format('d/m/Y', $request->input('date'));
		$date = date_format($date,'Y-m-d');
		$request->merge(array('date' => $date));
		$quote = $request->input('quote')?1:0;
		$request->merge(array('quote' => $quote));
		$event->fill($request->all());
		$event->save();

			//Session::flash('message','Usuario editado correctamente');
		  
		if($event){
			return redirect('/event')->with('success','Contenido editado correctamente');
		}else{
			return redirect('/event')->with('alert','Error no se pudo editar el contenido');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$event = Event::find($id);
		$event->state = -1;
    	$event->save();
		return redirect('/event')->with('success','Contenido eliminado correctamente');
	}

	public function approved($id)
	{
		if(Event::where('id', $id)->update(array('state' => 1))){
			
			###################################
			## Envio Noficicacion a FireBase ##
			###################################

			$event = Event::find($id);


			$politician_id = $event->politician_id;

			$politician = Politician::find($politician_id);

			$title = 'Nueva Información de '.$politician->name;

			$in = $event->description;
			$count = 10;
			$body = strlen($in) > $count ? substr($in,0,$count)."..." : $in;


			$optionBuilder = new OptionsBuilder();
			$optionBuilder->setTimeToLive(60*20);


			$notificationBuilder = new PayloadNotificationBuilder($title);
			$notificationBuilder->setBody($body)
							    ->setSound('default')
							    ->setColor("#ff6f00")
							    ->setClickAction("ver_politico")
							    ->setIcon('ic_notification_on');
							    
			$dataBuilder = new PayloadDataBuilder();
			$dataBuilder->addData(['Politico' => $politician_id]);
			$dataBuilder->addData(['refresh' => 1]);

			$option = $optionBuilder->build();
			$notification = $notificationBuilder->build();
			$data = $dataBuilder->build();

			$tokens = Token_Firebase::pluck('token')->toArray();

			if(!empty($tokens)){

				$downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
				$downstreamResponse->numberSuccess();
				$downstreamResponse->numberFailure();
				$downstreamResponse->numberModification();

				//return Array - you must remove all this tokens in your database
				$downstreamResponse->tokensToDelete(); 

				//return Array (key : oldToken, value : new token - you must change the token in your database )
				$downstreamResponse->tokensToModify(); 

				//return Array - you should try to resend the message to the tokens in the array
				$downstreamResponse->tokensToRetry();
			}

			return redirect('/event')->with('success','Contenido Aprobado correctamente');
		}else{
			return redirect('/event')->with('alert','Error, no se pudo aprobar');
		}
	}

	public function tobeapproved($id)
	{
		if(Event::where('id', $id)->update(array('state' => 0))){
			return redirect('/event')->with('success','Contenido a revisión.');
		}else{
			return redirect('/event')->with('alert','Error, no se pudo aprobar');
		}
	}

	public function share(){
		$event = Event::where('share',0)->where('state',1)->with('Politician')->orderBy('date','desc')->first();

		if($event){

			$event->share = 1;

			$event->save();

			#########################
			## Publica en facebook ##
			#########################
			
			$fb = new \Facebook\Facebook([
				'app_id' => '1278649658918057',
				'app_secret' => 'b5b8c10dc37800f40259943fcd2df65c',
				'default_graph_version' => 'v2.9'
			]);

			$link = action('PoliticianController@profile',['name'=> $event->Politician->slug]);

			$intro = "Nueva información de ";
			$fin = "Toda la información de los Candidatos aquí";

			$message = $intro.$event->Politician->name.":"."\n".$event->description."."."\n".$fin;

			$linkData = [
				'link' => $link,
				'message' => $message
			];

			$pageAccessToken ='EAASK7NK2RKkBABYmR59OYJaw1ZC2kUG81jrmlpHtAB9Gas384qZC5ZBrXUvB24P3PcZBjkSCRziZA8wFGw3ysewvhu4Vmus5AOQwLwJCKpbEkyoalf3IDucvqKVZCHQNGjDkgSggq5XpZBF8MFcuZAPCSo4WuijpOR8ZD';

			try {
				$response = $fb->post('/me/feed', $linkData, $pageAccessToken);
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
				echo 'Graph returned an error: '.$e->getMessage();
			exit;
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				echo 'Facebook SDK returned an error: '.$e->getMessage();
			exit;
			}

			$graphNode = $response->getGraphNode();

			########################
			## Publica en twitter ##
			########################

			$url = app('bitly')->getUrl($link);

			$fin = "Toda la info en ";
			$intro = $intro.$event->Politician->name.": ";
			$fin = ", ".$fin.$url;

			$countIntro = strlen($intro);
			$countFin = strlen($fin);


			$count = 130 - ( $countIntro + $countFin ) ;

			$in = $event->description;

			$out = strlen($in) > $count ? substr($in,0,$count)."..." : $in;

			$status = $intro.$out.$fin;
			//$status = $fin;

			try
			{
				$response = \Thujohn\Twitter\Facades\Twitter::postTweet(['status' => $status, 'format' => 'json']);
			}
			catch (Exception $e)
			{
			}

			echo "Evento ".$event->id." compartido";

		}else{
			echo "No se econtró ningún evento";
		}
	}

	
}
