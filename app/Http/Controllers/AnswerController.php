<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Politician;
use App\Answer;

class AnswerController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$questions = Question::All();
		$politicians = Politician::orderBy('name','asc')->pluck('name','id');
		return View('answers.index',compact('questions','politicians'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$questions = Question::All();
		return View('answers.create',compact('questions'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
		dd($request->input('politician_id'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	public function saveAnswer(){

		if(Answer::where('question_id', $_GET['id'])->where('politician_id', $_GET['politician_id'])->update(array('value' => $_GET['valor'],'description' => $_GET['description']))){
			exit(json_encode(1));
		}else{
			$answer = Answer::create([
				'question_id' => $_GET['id'],
				'politician_id' => $_GET['politician_id'],
				'value' => $_GET['valor'],
				'description' => $_GET['description']
			]);


			if(!empty($answer)){
				exit(json_encode(1));
			}else{
				exit(json_encode(0));
			}
		}

	}

	public function infoPolitician(){
		$answersPolitician = Answer::where('politician_id',$_GET['id'])->get();
		exit(json_encode($answersPolitician));
	}

	public function response($politician_id,$hash){
		$questions = Question::All();
		$politician = Politician::where('id' , $politician_id)->where('hash',$hash)->first();
		if(!$politician){
			return redirect('/')->with('alert','Error con lo datos del candidato.');
		}

		return View("answers.response",compact('politician','questions','politician_id','hash'));
	}
}
