<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Instance;
use App\Politician;

class InstanceController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$instances = Instance::orderBy('title')->with(['registries'])->get();

		return View('instances.index',compact('instances'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return View('instances.create');
	}

	public function addregistry($id){
		$politicians = Politician::orderBy('name','asc')->pluck('name','id');
		$instance = Instance::find($id);
		return View('instances.addregistry',compact('politicians','instance'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		$instance = Instance::create([
			'title' => $request->input('title'),
			'description' => $request->input('description'),
			'photo' => $request->input('photo'),
			'year' => $request->input('year'),
		]);

		if(!empty($instance)){
			return redirect('/instances')->with('success','Se ha creado correctamente el caso.');
		}else{
			return redirect('/instances')->with('alert','Error no se pudo agregar el caso');
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$instance = Instance::find($id);
		return view('instances.show',compact('instance'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$instance = Instance::find($id);
		return view('instances.edit',compact('instance'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$instance = Instance::find($id);
		$instance->fill($request->all());
		$instance->save();

			//Session::flash('message','Usuario editado correctamente');
		  
		if($instance){
			return redirect('/instances')->with('success','Caso editado correctamente');
		}else{
			return redirect('/instances')->with('alert','Error no se pudo editar el caso');
		}
	}

	public function registries($id)
	{
		$instance = Instance::where('id',$id)->with(['registries'])->first();

		return View('instances.registries',compact('instance'));
	}

	public function approved($id)
	{
		if(Instance::where('id', $id)->update(array('state' => 1))){
			return redirect('/instances')->with('success','Contenido Aprobado correctamente');
		}else{
			return redirect('/instances')->with('alert','Error, no se pudo aprobar');
		}
	}

	public function tobeapproved($id)
	{
		if(Instance::where('id', $id)->update(array('state' => 0))){
			return redirect('/instances')->with('success','Contenido a revisión.');
		}else{
			return redirect('/instances')->with('alert','Error, no se pudo aprobar');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
			//
	}
	
}
