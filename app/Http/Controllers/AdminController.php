<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;
use Illuminate\Support\Facades\Auth;



class AdminController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function listUsers()
	{
		if( Auth::user()->type != 1){
			return redirect('/event')->with('alert','No tiene permisos de administración');
		}
		$users  = User::All();
		return view('admin.listUsers',compact('users'));
	}

	public function be($id,$type)
	{
		if( Auth::user()->type == 1){
			$user = User::find($id);
			$user->type = $type;
			if($user->save()){
				return redirect('/admin/listUsers')->with('success','Se ha cambiado el estado del usuario.');
			}else{
				return redirect('/admin/listUsers')->with('alert','Error no se pudo cambiar el estado del usuario');
			}
		}else{
			return redirect('/admin/listUsers')->with('alert','Error no se pudo cambiar el estado del usuario porque no eres administrador');
		}
	}
}
