<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="Historia Política | No olvides lo que han dicho los políticos en el tiempo">
		<meta name="author" content="weblabs.cl">

		<meta property="og:title" content="Historia Política | No olvides lo que han dicho los políticos en el tiempo"/>
		<meta property="og:image" content="https://scontent.fscl1-1.fna.fbcdn.net/v/t1.0-9/17862833_111172946102757_498457469857243312_n.jpg?oh=06c0c821583f7d717b862ac15d47fa83&oe=59528660"/>
		<meta property="og:site_name" content="Historia Política"/>
		<meta property="og:description" content="Muchos tenemos una memoria a corto plazo y se nos olvida información que puede ser de utilidad antes de confiar en un próximo candidato, acá te entregamos información sobre los candidatos y algunas citas o declaraciones realizadas en el trancurso de los años."/>
		<meta property="og:url" content="http://www.historiapolitica.cl"/>
		<meta property="fb:app_id" content="1278649658918057"/>
		<link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}" />

		<title>Historial - @yield('title')</title>

		<!-- Bootstrap core CSS -->
		{!!Html::style('css/app.css')!!}
		{!!Html::style('css/admin.css')!!}
		{!!Html::style('vendor/bootstrap/css/bootstrap.min.css')!!}


		<!-- Custom fonts for this template -->
		{!!Html::style('vendor/font-awesome/css/font-awesome.min.css')!!}
		

		<!-- Plugin CSS -->
		 {!!Html::style('vendor/magnific-popup/magnific-popup.css')!!}

		<!-- Custom styles for this template -->
		{!!Html::style('css/creative.min.css')!!}

		{!!Html::style('css/jquery.dataTables.min.css')!!}
		




		<!-- Temporary navbar container fix -->
		<style>
		.navbar-toggler {
			z-index: 1;
		}
		
		@media (max-width: 576px) {
			nav > .container {
				width: 100%;
			}
		}
		</style>

		{!!Html::script('js/app.js')!!}
		<!-- Bootstrap core JavaScript -->
		
		{!!Html::script('vendor/tether/tether.min.js')!!}

		<!-- Plugin JavaScript -->
		{!!Html::script('vendor/jquery-easing/jquery.easing.min.js')!!}
		{!!Html::script('vendor/scrollreveal/scrollreveal.min.js')!!}
		{!!Html::script('vendor/magnific-popup/jquery.magnific-popup.min.js')!!}


		<!-- Custom scripts for this template -->
		{!!Html::script('js/creative.min.js')!!}

		<!-- Valida Formulario -->
		{!!Html::script('js/jquery.validate.min.js')!!}
		{!!Html::script('js/inputmask.js')!!}
		{!!Html::script('js/jquery.inputmask.js')!!}
		{!!Html::script('js/inputmask.date.extensions.js')!!}
		{!!Html::script('js/jquery.dataTables.min.js')!!}

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-97492444-1', 'auto');
			ga('send', 'pageview');

			

		</script>

	</head>

	<body id="page-top">

		<!-- Navigation -->
		<nav id="mainNav" class="navbar navbar-fixed-top">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <i class="fa fa-bars"></i>
					</button>
					<a class="navbar-brand page-scroll" href="/">Historia Política</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="#nada">Bienvenido <b>{{ Auth::user()->name }}</b></a>
						</li>
						<li>
							<?=link_to_action('InstanceController@create', $title = 'Agregar Caso', $parameters = [], $attributes = []);?>
						</li>
						<li>
							<?=link_to_action('InstanceController@index', $title = 'Listado Casos', $parameters = [], $attributes = []);?>
						</li>
						<li>
							<?=link_to_action('PoliticianController@create', $title = 'Agregar Candidato', $parameters = [], $attributes = []);?>
						</li>
						<li>
							<?=link_to_action('PoliticianController@showList', $title = 'Listado Candidatos', $parameters = [], $attributes = []);?>
						</li>
						<li>
							<?=link_to_action('EventController@index', $title = 'Administrar Acontecimientos', $parameters = [], $attributes = []);?>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Encuestas <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><?=link_to_action('QuestionController@index', $title = 'Listado', $parameters = [], $attributes = []);?></li>
								<li><?=link_to_action('QuestionController@create', $title = 'Agregar Item', $parameters = [], $attributes = []);?></li>
								<li><?=link_to_action('QuestionController@graphics', $title = 'Estadísticas', $parameters = [], $attributes = []);?></li>
								<li role="separator" class="divider"></li>
								<li class="dropdown-header">Candidatos</li>
								<li><?=link_to_action('AnswerController@index', $title = 'Respuesta de Candidatos', $parameters = [], $attributes = []);?></li>
							</ul>
						</li>
						<li>
							<a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Salir</a>
						</li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>
		
		

		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div  style="margin-top:80px;margin-bottom: 50px">
						@if (Session::has('success'))
							<div class="alert alert-success">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> {{ Session::get('success') }}</div>
						@endif
						@if (Session::has('alert'))
							<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> {{ Session::get('alert') }}</div>
						@endif
						@yield('content')
					</div>
				</div>
			</div>
		</div>
	   
		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			{{ csrf_field() }}
		</form>

		<script>

		!function($) {
	    		var defaults = {
				"bSort": true,
				"bProcessing": true,
				"language": {
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ registros",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
					"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				}
			};

			$('.datatables').DataTable( $.extend( true, {}, defaults, {
				"bFilter": true
			} ) );

			$(".datatables").DataTable()
		}(jQuery);
		
	</script>
	</body>
</html>
