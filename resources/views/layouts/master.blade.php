<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="Historia Política | No olvides lo que han dicho los políticos en el tiempo">
		<meta name="author" content="weblabs.cl">

		<meta property="og:title" content="Historia Política | No olvides lo que han dicho los políticos en el tiempo"/>
		<meta property="og:image" content="https://scontent.fscl1-1.fna.fbcdn.net/v/t1.0-9/17862833_111172946102757_498457469857243312_n.jpg?oh=06c0c821583f7d717b862ac15d47fa83&oe=59528660"/>
		<meta property="og:site_name" content="Historia Política"/>
		<meta property="og:url" content="http://www.historiapolitica.cl"/>
		<meta property="fb:app_id" content="1278649658918057"/>
		<meta property="og:description" content="Muchos tenemos una memoria a corto plazo y se nos olvida información que puede ser de utilidad antes de confiar en un próximo candidato, acá te entregamos información sobre los candidatos y algunas citas o declaraciones realizadas en el trancurso de los años."/>

		<link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}" />

		<title>Historial - @yield('title')</title>

		<!-- Bootstrap core CSS -->
		{!!Html::style('vendor/bootstrap/css/bootstrap.min.css')!!}


		<!-- Custom fonts for this template -->
		{!!Html::style('vendor/font-awesome/css/font-awesome.min.css')!!}
		

		<!-- Plugin CSS -->
		 {!!Html::style('vendor/magnific-popup/magnific-popup.css')!!}

		<!-- Custom styles for this template -->
		{!!Html::style('css/creative.min.css')!!}
		{!!Html::style('css/app.css')!!}
		


		<!-- Temporary navbar container fix -->
		<style>
		.navbar-toggler {
			z-index: 1;
		}
		
		@media (max-width: 576px) {
			nav > .container {
				width: 100%;
			}
		}
		</style>

		{!!Html::script('js/app.js')!!}

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-97492444-1', 'auto');
			ga('send', 'pageview');

		</script>

		<script>
		var root = "{{ URL::to('/')."/" }}"
		</script>

	</head>

	<body id="page-top">

		<!-- Navigation -->
		<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
	        <div class="container-fluid">
	            <!-- Brand and toggle get grouped for better mobile display -->
	            <div class="navbar-header">
	                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	                    <span class="sr-only">Toggle navigation</span> <i class="fa fa-bars"></i>
	                </button>
	                <a class="navbar-brand page-scroll" href="#page-top">Historia Política</a>
	            </div>

	            <!-- Collect the nav links, forms, and other content for toggling -->
	            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	                <ul class="nav navbar-nav navbar-right">
	                    <li>
	                        <a class="page-scroll" href="#candidatos">Candidatos</a>
	                    </li>
	                    <li>
	                        <a class="page-scroll" href="#encuesta">Colaborar</a>
	                    </li>
	                    <li>
	                        <a class="page-scroll" href="#contact">Contacto</a>
	                    </li>
	                    <li>
	                        <a href="login">Ingresar</a>
	                    </li>
	                </ul>
	            </div>
	            <!-- /.navbar-collapse -->
	        </div>
	        <!-- /.container-fluid -->
	    </nav>
		<header>
	        <div class="header-content">
	            <div class="header-content-inner">
	                <h1 id="homeHeading">Nunca olvidemos lo que hacen o dicen</h1>
	                <hr>
		            <div class="row">
		                <div class="col-lg-12 text-center">
		                    <p class="text-faded">Muchos tenemos una memoria a corto plazo y se nos olvida información que puede ser de utilidad antes de confiar en un próximo candidato, acá te entregamos información sobre los candidatos y algunas citas o declaraciones realizadas en el trascurso de los años.</p>
		                    <a href="#candidatos" class="page-scroll btn btn-primary btn-xl sr-button">Refresca tu memoria ahora!</a>
		                </div>
		            </div>
	            </div>
	        </div>
   		</header>

	    <section id="candidatos">
	        <div class="container">
	            <div class="row">
	                <div class="col-lg-12 col-md-12">
                        <h2 class="section-heading text-center">Candidatos Presidenciales</h2>
						<hr class="primary">
						@yield('content')
	                </div>
	            </div>
	        </div>
	    </section>


		<section class="no-padding" id="portfolio">
			<div class="container-fluid">
				<!-- <div class="row no-gutter popup-gallery">
					<div class="col-lg-4 col-sm-6">
						<a class="portfolio-box" href="img/portfolio/fullsize/1.jpg">
							<img class="img-fluid" src="img/portfolio/thumbnails/1.jpg" alt="">
							<div class="portfolio-box-caption">
								<div class="portfolio-box-caption-content">
									<div class="project-category text-faded">
										Category
									</div>
									<div class="project-name">
										Project Name
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-sm-6">
						<a class="portfolio-box" href="img/portfolio/fullsize/2.jpg">
							<img class="img-fluid" src="img/portfolio/thumbnails/2.jpg" alt="">
							<div class="portfolio-box-caption">
								<div class="portfolio-box-caption-content">
									<div class="project-category text-faded">
										Category
									</div>
									<div class="project-name">
										Project Name
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-sm-6">
						<a class="portfolio-box" href="img/portfolio/fullsize/3.jpg">
							<img class="img-fluid" src="img/portfolio/thumbnails/3.jpg" alt="">
							<div class="portfolio-box-caption">
								<div class="portfolio-box-caption-content">
									<div class="project-category text-faded">
										Category
									</div>
									<div class="project-name">
										Project Name
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-sm-6">
						<a class="portfolio-box" href="img/portfolio/fullsize/4.jpg">
							<img class="img-fluid" src="img/portfolio/thumbnails/4.jpg" alt="">
							<div class="portfolio-box-caption">
								<div class="portfolio-box-caption-content">
									<div class="project-category text-faded">
										Category
									</div>
									<div class="project-name">
										Project Name
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-sm-6">
						<a class="portfolio-box" href="img/portfolio/fullsize/5.jpg">
							<img class="img-fluid" src="img/portfolio/thumbnails/5.jpg" alt="">
							<div class="portfolio-box-caption">
								<div class="portfolio-box-caption-content">
									<div class="project-category text-faded">
										Category
									</div>
									<div class="project-name">
										Project Name
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-sm-6">
						<a class="portfolio-box" href="img/portfolio/fullsize/6.jpg">
							<img class="img-fluid" src="img/portfolio/thumbnails/6.jpg" alt="">
							<div class="portfolio-box-caption">
								<div class="portfolio-box-caption-content">
									<div class="project-category text-faded">
										Category
									</div>
									<div class="project-name">
										Project Name
									</div>
								</div>
							</div>
						</a>
					</div>
				</div> -->
			</div>
		</section>

		<section id="encuesta" class="bg-primary">
	        <div class="container text-center">
	            <div class="call-to-action">
	                <h2>¿Tienes Información?</h2>
	                <p>Colabora con datos para que así el resto de los ciudadanos pueda informarse.</p>
	                <a href="/event/create" class="btn btn-default btn-xl sr-button">Colaborar!</a>
	            </div>
	        </div>
	    </section>

	    <section id="contact" class="bg-dark">
	        <div class="container">
	            <div class="row">
	                <div class="col-lg-8 col-lg-offset-2 text-center">
	                    <h2 class="section-heading">¿Quieres ayudar?</h2>
	                    <hr class="primary">
	                    <p>Si quieres ayudarnos con ideas o con ser parte de este proyecto envíanos un mensaje.</p>
	                </div>
	                <div class="col-lg-4 text-center">
	                    <i class="fa fa-envelope fa-3x sr-contact"></i>
	                    <p><a href="mailto:info@historiapolitica.cl">info@historiapolitica.cl</a></p>
	                </div>
	                <div class="col-lg-4 text-center">
	                    <i class="fa fa-facebook-official fa-3x sr-contact"></i>
	                    <p><a href="https://www.facebook.com/historiapoliticacl/" target="_blank">/historiapoliticacl</a></p>
	                </div>
	                <div class="col-lg-4 text-center">
	                    <i class="fa fa-twitter-square fa-3x sr-contact"></i>
	                    <p><a href="https://twitter.com/histopolitica" target="_blank">@histopolitica</a></p>
	                </div>
	            </div>
	        </div>
	    </section>

		<!-- Bootstrap core JavaScript -->
		
		{!!Html::script('vendor/tether/tether.min.js')!!}

		<!-- Plugin JavaScript -->
		{!!Html::script('vendor/jquery-easing/jquery.easing.min.js')!!}
		{!!Html::script('vendor/scrollreveal/scrollreveal.min.js')!!}
		{!!Html::script('vendor/magnific-popup/jquery.magnific-popup.min.js')!!}


		<!-- Custom scripts for this template -->
		{!!Html::script('js/creative.min.js')!!}
		
		{!!Html::script('js/messenger.min.js')!!}

		{!!Html::script('js/app2.js')!!}
		
	</body>
</html>
