<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="Historia Política | No olvides lo que han dicho los políticos en el tiempo">
		<meta name="author" content="weblabs.cl">
		
		@if (trim($__env->yieldContent('og_title')))  
			<meta property="og:title" content="{{$__env->yieldContent('og_title')}}"/>
		@else
			<meta property="og:title" content="Historia Política | No olvides lo que han dicho los políticos en el tiempo"/>
		@endif

		@if (trim($__env->yieldContent('og_description')))   
			<meta property="og:description" content="{{$__env->yieldContent('og_description')}}"/>
		@else
			<meta property="og:description" content="Muchos tenemos una memoria a corto plazo y se nos olvida información que puede ser de utilidad antes de confiar en un próximo candidato, acá te entregamos información sobre los candidatos y algunas citas o declaraciones realizadas en el trancurso de los años."/>
		@endif

		@if (trim($__env->yieldContent('og_image')))  
			<meta property="og:image" content="{{$__env->yieldContent('og_image')}}"/>
		@else
			<meta property="og:image" content="https://scontent.fscl1-1.fna.fbcdn.net/v/t1.0-9/17862833_111172946102757_498457469857243312_n.jpg?oh=06c0c821583f7d717b862ac15d47fa83&oe=59528660"/>
		@endif

		<meta property="og:site_name" content="Historia Política"/>

		<link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}" />

		<meta property="og:url" content="http://www.historiapolitica.cl"/>
		<meta property="fb:app_id" content="1278649658918057"/>

		<title>Historial - @yield('title')</title>

		<!-- Bootstrap core CSS -->
		{!!Html::style('vendor/bootstrap/css/bootstrap.min.css')!!}



		<!-- Custom fonts for this template -->
		{!!Html::style('vendor/font-awesome/css/font-awesome.min.css')!!}
		

		<!-- Plugin CSS -->
		 {!!Html::style('vendor/magnific-popup/magnific-popup.css')!!}

		<!-- Custom styles for this template -->
		{!!Html::style('css/creative.min.css')!!}

		{!!Html::style('css/messenger.css')!!}
		
		{!!Html::style('css/messenger-theme-air.css')!!}
		
		{!!Html::style('css/app.css')!!}
		


		<!-- Temporary navbar container fix -->
		<style>
		.navbar-toggler {
			z-index: 1;
		}
		
		@media (max-width: 576px) {
			nav > .container {
				width: 100%;
			}
		}
		</style>

		{!!Html::script('js/app.js')!!}
		<!-- Bootstrap core JavaScript -->
		
		{!!Html::script('vendor/tether/tether.min.js')!!}

		<!-- Plugin JavaScript -->
		{!!Html::script('vendor/jquery-easing/jquery.easing.min.js')!!}
		{!!Html::script('vendor/scrollreveal/scrollreveal.min.js')!!}
		{!!Html::script('vendor/magnific-popup/jquery.magnific-popup.min.js')!!}


		<!-- Custom scripts for this template -->
		{!!Html::script('js/creative.min.js')!!}

		<!-- Valida Formulario -->
		{!!Html::script('js/jquery.validate.min.js')!!}
		{!!Html::script('js/inputmask.js')!!}
		{!!Html::script('js/jquery.inputmask.js')!!}
		{!!Html::script('js/inputmask.date.extensions.js')!!}

		{!!Html::script('js/messenger.min.js')!!}
		
		{!!Html::script('js/app2.js')!!}
		
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-97492444-1', 'auto');
			ga('send', 'pageview');

		</script>

		<script>
		var root = "{{ URL::to('/')."/" }}"
		</script>

	</head>

	<body id="page-top">

		<!-- Twitter Api -->
		<script>window.twttr = (function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0],
		    t = window.twttr || {};
		  if (d.getElementById(id)) return t;
		  js = d.createElement(s);
		  js.id = id;
		  js.src = "https://platform.twitter.com/widgets.js";
		  fjs.parentNode.insertBefore(js, fjs);

		  t._e = [];
		  t.ready = function(f) {
		    t._e.push(f);
		  };

		  return t;
		}(document, "script", "twitter-wjs"));</script>

		<!-- Fb Api -->
		<script type="text/javascript">
		window.fbAsyncInit = function() {
			FB.init({
			appId      : '1278649658918057',
			xfbml      : true,
			version    : 'v2.9'
			});
			FB.AppEvents.logPageView();
		};

		(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		

		function postToFeed(name, image,percent){

			FB.api(
			    "/me/news.publishes",
			    function (response) {
			      if (response && !response.error) {
			        /* handle the result */
			      }
			    }
			);

			var obj = {
				method: 'share_open_graph',
                action_type: 'og.likes',
				hashtag: '#presidenciales2017',
                action_properties: JSON.stringify({
                    object: {
                        "url" : root+'poll',
                        "title": 'Con un '+percent+'%  de coincidencia mi candidato es : '+name,
                        "description": 'Descubre el tuyo en www.historiapolitica.cl',
                        "image": image
                    }
                })
			}

			function callback(response){}
			FB.ui(obj, callback);
		}
		</script>

		<!-- Navigation -->
		<nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-color">
	        <div class="container-fluid">
	            <!-- Brand and toggle get grouped for better mobile display -->
	            <div class="navbar-header">
	                
	                <a class="navbar-brand page-scroll" href="/"><i class="fa fa-chevron-left" aria-hidden="true"></i> Historia Política</a>
	            </div>

	            <!-- Collect the nav links, forms, and other content for toggling -->
	            
	            <!-- /.navbar-collapse -->
	        </div>
	        <!-- /.container-fluid -->
	    </nav>
		
	    <section id="candidatos">
	        <div class="container">
	            <div class="row">
	                <div class="col-lg-12 col-md-12 text-center">
	                	@if (Session::has('success'))
							<div class="alert alert-success">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> {{ Session::get('success') }}</div>
						@endif
						@if (Session::has('alert'))
							<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> {{ Session::get('alert') }}</div>
						@endif
						@yield('content')
	                </div>
	            </div>
	        </div>
	    </section>
		<section id="encuesta" class="bg-primary">
	        <div class="container text-center">
	            <div class="call-to-action">
	                <h2>¿Tienes Información?</h2>
	                <p>Colabora con datos para que así el resto de los ciudadanos pueda informarse.</p>
	                <a href="/event/create" class="btn btn-default btn-xl sr-button">Colaborar!</a>
	            </div>
	        </div>
	    </section>
	    <section id="contact" class="bg-dark">
	        <div class="container">
	            <div class="row">
	                <div class="col-lg-8 col-lg-offset-2 text-center">
	                    <h2 class="section-heading">¿Quieres ayudar?</h2>
	                    <hr class="primary">
	                    <p>Si quieres ayudarnos con ideas o con ser parte de este proyecto envíanos un mensaje.</p>
	                </div>
	                <div class="col-lg-4 text-center">
	                    <i class="fa fa-envelope fa-3x sr-contact"></i>
	                    <p><a href="mailto:info@historiapolitica.cl">info@historiapolitica.cl</a></p>
	                </div>
	                <div class="col-lg-4 text-center">
	                    <i class="fa fa-facebook-official fa-3x sr-contact"></i>
	                    <p><a href="https://www.facebook.com/historiapoliticacl/" target="_blank">/historiapoliticacl</a></p>
	                </div>
	                <div class="col-lg-4 text-center">
	                    <i class="fa fa-twitter-square fa-3x sr-contact"></i>
	                    <p><a href="https://twitter.com/histopolitica" target="_blank">@histopolitica</a></p>
	                </div>
	            </div>
	        </div>
	    </section>
	</body>
</html>
