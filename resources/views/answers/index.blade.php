@extends('layouts.admin')
@section('title', 'Agregar Item Encuesta')
@section('content')


<fieldset style="text-align:left !important">

	<!-- Form Name -->
	<legend>Repuestas Candidatos a Encuesta</legend>

	{!! Form::open(array('url' => '/question', 'method' => 'post','id' => 'AnswerFormCreate')) !!}
		
		<div class="row">
			<div class="col-md-12">
				{!!Form::label('Candidato ')!!}
				 <?php $c = (!isset($event))? "":$event->politician_id;  ?>
				{!!Form::select('politician_id',$politicians, $c ,['class' => 'form-control required','placeholder' => 'Seleccione Candidato'])!!}
			</div>
			<div class="col-md-12">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-top:30px">
					@foreach($questions as $question) 
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading_question_{{$question->id}}">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_question_{{$question->id}}" aria-expanded="false" aria-controls="collapse_question_{{$question->id}}">
										{{$question->description}}
									</a>
								</h4>
							</div>
							<div id="collapse_question_{{$question->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_question_{{$question->id}}">
								<div class="panel-body">
									<input type="radio" data-id="{{$question->id}}" name="question_{{$question->id}}" value="1" id="radio_{{$question->id}}_1" disabled="disabled"><label for="radio_{{$question->id}}_1"> Si</label>
									<input type="radio" data-id="{{$question->id}}"  name="question_{{$question->id}}" value="2"  id="radio_{{$question->id}}_0" disabled="disabled" style="margin-left:20px"><label for="radio_{{$question->id}}_0"> No</label>
									<h6>Puedes fundamentar tu decisión en 140 caracteres.</h6>
									<textarea disabled="disabled" data-id="{{$question->id}}" class="form-control" name="question_description_{{$question->id}}" style="width:100%;height:70px" cols="30" rows="10"></textarea>
								</div>
							</div>
						</div>
					@endforeach
				</div>
				<!-- <table class="table">
					<thead>
						<tr>
							<th>
								Pregunta
							</th>
							<th>
								A favor
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach($questions as $question) 
						<tr>
							<td>{{$question->description}}</td>
							<td>
								<input type="radio" data-id="{{$question->id}}" name="question_{{$question->id}}" value="1" disabled="disabled"> Si
								<input type="radio" data-id="{{$question->id}}"  name="question_{{$question->id}}" value="2"  disabled="disabled" style="margin-left:20px"> No
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
									 Fundamentar votación
								</a>
								<div class="collapse" id="collapseExample">
									<div class="well">
										
									</div>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table> -->
			</div>
		</div>
		
	{!! Form::close() !!}

	</fieldset>

	<script>
		!function($) {
	    		$("#QuestionFormCreate").validate()

	    		$("select[name=politician_id]").change(function(){
	    			$("input[type=radio]").prop("checked",false)	
	    			$("textarea").val("")	
	    			if($(this).val() != ""){
	    				$("input[type=radio]").removeAttr("disabled")
	    				$("textarea").removeAttr("disabled")
	    				$.ajax({
	    					method: "GET",
	    					url: "/answer/infoPolitician",
	    					data: { id: $(this).val()},
	    					dataType: "json"
	    				})
	    				.done(function( data ) {
	    					if(data != null){
	    						$.each(data,function(i,item){
	    							console.log(item);
	    							$("input[name=question_"+item.question_id+"][value="+item.value+"]").prop("checked",true)
	    							if(typeof item.description != "undefined"){
	    								$("textarea[name=question_description_"+item.question_id+"]").val(item.description)
	    							}
	    						})
	    					}
	    				});
	    			}else{
	    				
	    				$("input[type=radio]").attr("disabled","disabled")
	    			}
	    		})

	    		$("input[type=radio],textarea").change(function(){
	    			id = $(this).attr("data-id")
	    			valor = $("input[name=question_"+id+"]:checked").val()
	    			description = $("textarea[name=question_description_"+id+"]").val()
	    			politician_id = $("select[name=politician_id]").val()
	    			$.ajax({
	    				method: "GET",
	    				url: "/answer/saveAnswer",
	    				data: {id:id,valor:valor,politician_id:politician_id,description: description},
	    				dataType: "json"
	    			})
	    			.done(function( data ) {
	    				console.log(data)
	    				if(data != 1){
	    					alert("Error,no se pudo agregar votación");
	    				}
	    			});
	    		});

		}(jQuery);
	</script>

	<style>
		.panel-default>.panel-heading {
			color: #fff;
			background-color: #f05f40;
			border-color: #ff2d00;
			border-radius: 0px;
			padding: 20px;
		}
		.panel-default a:hover, .panel-default a:focus{
			color: #FFF;
		}
		.panel-default>.panel-heading+.panel-collapse>.panel-body {
			border-top-color: #f05f40;
			background: #ededed;
			border: 1px solid #f05f40;
		}
	</style>
	
@endsection

