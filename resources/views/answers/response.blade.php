@extends('layouts.public')
@section('title', 'Encuesta')
@section('content')


<fieldset style="text-align:left !important">

	<!-- Form Name -->
	<legend>Bienvenido/a {{$politician->name}}</legend>
	<h2><span class="small">Favor contestar las siguientes preguntas</span></h2>

	{!! Form::open(array('url' => '/question', 'method' => 'post','id' => 'AnswerFormCreate')) !!}
		
		<div class="row">
			{!!Form::hidden('politician_id',$politician_id)!!}
			{!!Form::hidden('hash',$hash)!!}
			<div class="col-md-12">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-top:30px">
					@foreach($questions as $question) 
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading_question_{{$question->id}}">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_question_{{$question->id}}" aria-expanded="false" aria-controls="collapse_question_{{$question->id}}">
										{{$question->description}} <b style="float: right"><span class="answers_text" id="answer_to_{{$question->id}}"></span></b>
									</a>
								</h4>
							</div>
							<div id="collapse_question_{{$question->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_question_{{$question->id}}">
								<div class="panel-body">
									<input type="radio" data-id="{{$question->id}}" name="question_{{$question->id}}" value="1" id="radio_{{$question->id}}_1" ><label for="radio_{{$question->id}}_1"> Si</label>
									<input type="radio" data-id="{{$question->id}}"  name="question_{{$question->id}}" value="2"  id="radio_{{$question->id}}_0"  style="margin-left:20px"><label for="radio_{{$question->id}}_0"> No</label>
									<h6>Puedes fundamentar tu decisión en 140 caracteres.</h6>
									<textarea data-id="{{$question->id}}" class="form-control" name="question_description_{{$question->id}}" style="width:100%;height:70px" cols="30" rows="10"></textarea>
								</div>
							</div>
						</div>
					@endforeach
				</div>
				<!-- <table class="table">
					<thead>
						<tr>
							<th>
								Pregunta
							</th>
							<th>
								A favor
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach($questions as $question) 
						<tr>
							<td>{{$question->description}}</td>
							<td>
								<input type="radio" data-id="{{$question->id}}" name="question_{{$question->id}}" value="1" disabled="disabled"> Si
								<input type="radio" data-id="{{$question->id}}"  name="question_{{$question->id}}" value="2"  disabled="disabled" style="margin-left:20px"> No
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
									 Fundamentar votación
								</a>
								<div class="collapse" id="collapseExample">
									<div class="well">
										
									</div>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table> -->
			</div>
		</div>
		
	{!! Form::close() !!}

	</fieldset>

	<script>
		!function($) {
	    		$("#QuestionFormCreate").validate()

	    		$.ajax({
				method: "GET",
				url: "/answer/infoPolitician",
				data: { id: "<?= $politician_id;?>"},
				dataType: "json"
			})
			.done(function( data ) {
				$(".answers_text").html("Sin Responder")
				if(data != null){
					$.each(data,function(i,item){
						console.log(item);
						$("input[name=question_"+item.question_id+"][value="+item.value+"]").prop("checked",true)
						if(item.value == 1){
							$("#answer_to_"+item.question_id).html("Respuesta: Si")
						}else if(item.value == 2){
							$("#answer_to_"+item.question_id).html("Respuesta: No")
						}
						if(typeof item.description != "undefined"){
							$("textarea[name=question_description_"+item.question_id+"]").val(item.description)
						}
					})
				}
			});

	    		$("input[type=radio],textarea").change(function(){
	    			id = $(this).attr("data-id")
	    			valor = $("input[name=question_"+id+"]:checked").val()
	    			if(typeof valor != "undefined"){
	    				description = $("textarea[name=question_description_"+id+"]").val()
		    			politician_id = $("input[name=politician_id]").val()
		    			if(valor == 1){
						$("#answer_to_"+id).html("Respuesta: Si")
					}else if(valor == 2){
						$("#answer_to_"+id).html("Respuesta: No")
					}
		    			$.ajax({
		    				method: "GET",
		    				url: "/answer/saveAnswer",
		    				data: {id:id,valor:valor,politician_id:politician_id,description: description},
		    				dataType: "json"
		    			})
		    			.done(function( data ) {
		    				console.log(data)
		    				if(data != 1){
		    					flash_error("Error,no se pudo agregar votación.")
		    				}else{
		    					flash_success("Votación grabada correctamente.")
		    				}
		    			});
	    			}else{
		    			flash_error("No olvide en seleccionar si o no.")
	    			}
	    		});

		}(jQuery);
	</script>

	<style>
		.panel-default>.panel-heading {
			color: #fff;
			background-color: #f05f40;
			border-color: #ff2d00;
			border-radius: 0px;
			padding: 20px;
		}
		.panel-default a:hover, .panel-default a:focus{
			color: #FFF;
		}
		.panel-default>.panel-heading+.panel-collapse>.panel-body {
			border-top-color: #f05f40;
			background: #ededed;
			border: 1px solid #f05f40;
		}
	</style>
	
@endsection

