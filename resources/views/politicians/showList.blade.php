@extends('layouts.admin')
@section('title', 'Listado Politicos')
@section('content')


<div class="container">
	<div class="row">
		<div class="col-md-12">
			
			<fieldset>
				<legend>Listado de Políticos</legend>
				<table class="table table-striped bg-info datatables">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Partido Político</th>
							<th>Acontecimientos</th>
							<th>Candidato a</th>
							<th>Opción</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($politicians as $politician) 
							<tr>
								<td>{{ $politician->name }}</td>
								<td>{{ $politician->politicalparty }}</td>
								<td style="text-align:center"><p style="    margin-top: 7px;  margin-bottom: 0px;"><span class="label label-warning">{{ count($politician->events) }}</span></p></td>
								<td>
									@if( $politician->type  == 1)
										<p style="    margin-top: 7px;  margin-bottom: 0px;"><span class="label label-danger">Presidencial</span></p>
									@endif
								</td>
								<td>

									{{ link_to_action('PoliticianController@edit', $title = 'Editar', $parameters = ["id" => $politician->id], $attributes = ['class' => 'btn btn-danger btn-sm'])  }}  
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</fieldset>
		</div>
	</div>
</div>


@endsection

