@extends('layouts.master')
@section('title','Inicio')
@section('content')
    <div class="politicians">
        @foreach ($politicians as $politician)
		    <div class="politician">
		    	<span class="close-politician"></span>
				<img src="{{asset('img/pin.png')}}" alt="Pin" class="pin">
				<div class="frame">
					<a href="/politician/profile/{{str_slug($politician->name)}}"><img src="{{ asset('img/candidatos/'.$politician->photo) }}" alt="" class="photo_polaroid"></a>
				</div>
				<div class="photo_name text-center">
					<span>{{ $politician->name }}</span>
				</div>
			</div>
		@endforeach
	</div>
<script>

	/*!function($) {
    	$('.politician>.frame>a').click(function() {
    		var parent = $(this).parent().parent()

    		if(!parent.hasClass('expand')){
    			parent.addClass('expand')
    			$(".politician:not(.politician.expand)").hide()
    		}

    	})
    	

    	$('.close-politician').click(function() {
    		$(this).parent().removeClass('expand');
    		$(".politician").show()
    	})
	}(jQuery);*/
	
</script>
@endsection