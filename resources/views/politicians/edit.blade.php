@extends('layouts.admin')
@section('title', 'Editar candidato')
@section('content')


<fieldset style="text-align:left !important">

	<!-- Form Name -->
	<legend>Editar Candidato</legend>

	{!! Form::model($politician,['route' => ['politician.update', $politician->id],'method' => 'PUT','id' => 'PoliticalFormEdit']) !!}
		@include('politicians.forms.politician')
		{!!Form::submit('Editar',['class' => 'btn btn-primary'])!!}
	{!! Form::close()!!}

	</fieldset>

	<script>
		!function($) {
	    		$("#PoliticalFormEdit").validate()
		}(jQuery);
	</script>

@endsection

