@extends('layouts.admin')
@section('title', 'Agregar candidato')
@section('content')


<fieldset style="text-align:left !important">

	<!-- Form Name -->
	<legend>Agregar Candidato</legend>

	{!! Form::open(array('url' => '/politician', 'method' => 'post','id' => 'PoliticalFormCreate')) !!}
		
		@include('politicians.forms.politician')
		{!! Form::submit('Agregar Candidato'); !!}
		
	{!! Form::close() !!}

	</fieldset>

	<script>
		!function($) {
	    		$("#PoliticalFormCreate").validate()
		}(jQuery);
	</script>

@endsection

