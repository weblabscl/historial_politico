@extends('layouts.public')
@section('title', $politician->name)
@section('og_title','Historia política de '.$politician->name)
@section('og_description','Información importante de '.$politician->name.' que la gente no debe olvidar, debe difundir.')
@section('og_image',asset('img/candidatos/'.$politician->photo))
@section('content')
<link rel="stylesheet" href="/css/rrssb.css" />
<script src="/js/rrssb.js"></script>
<link href="https://fonts.googleapis.com/css?family=Fira+Sans+Extra+Condensed:200,300,400,700" rel="stylesheet">
	
	<div class="profile">
		<div class="row">
			<div class="col-md-4" style="">
				<div class="left-profile">
					<center>
						<img src="{{ asset('img/candidatos/'.$politician->photo) }}" alt="{{$politician->name}}" class="img_profile">
					</center>
					<h2>{{$politician->name}}</h2>
					<h3>{{$politician->politicalparty}}</h3>
					<div class="description">
						@if(!empty($politician->school) AND $politician->school != "-")
							<p>Escuela: {{$politician->school}}</p>
						@endif
						@if(!empty($politician->profession))
							<p>Profesión: {{$politician->profession}}</p>
						@endif
						@if(!empty($politician->university->name))
							<p>Universidad: {{$politician->university->name}}</p>
						@endif
					</div>
				</div>
			</div>
			<div class="col-md-8" style="">
				<div class="right-profile">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#aceptados" aria-controls="aceptados" role="tab" data-toggle="tab">Acontecimientos</a></li>
						<li role="presentation"><a href="#aprobar" aria-controls="aprobar" role="tab" data-toggle="tab">Declaraciónes</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content scroll">
						<div role="tabpanel" class="tab-pane active" id="aceptados">
							@if(count($politician->events) > 0)
								<?php $count = 0;?>
								@foreach($politician->events as $e)
									@if($e->quote != 1)
										<?php $count++;?>
										<div class="blockquote">
											<a href="{{$e->link}}" target="_blank">
												<h5><span>{{ Carbon\Carbon::parse($e->date)->format('d/m/Y') }}</span> | {{$e->description}}</h5>
											</a>
										</div>
									@endif
								@endforeach
								@if($count == 0)
									<div class="blockquote">
										<h5>No hay acontecimientos, <b>ayúdanos</b> a buscar información.</h5>
									</div>
								@endif
							@else
								<div class="blockquote">
									<h5>No hay acontecimientos, <b>ayúdanos</b> a buscar información.</h5>
								</div>
							@endif
						</div>
						<div role="tabpanel" class="tab-pane" id="aprobar">
							@if(count($politician->events) > 0)
								<?php $count = 0;?>
								@foreach($politician->events as $e)
									@if($e->quote == 1)
										<?php $count++;?>
										<div class="blockquote">
											<a href="{{$e->link}}" target="_blank">
												<h5><span>{{ Carbon\Carbon::parse($e->date)->format('d/m/Y') }}</span> | {{$e->description}}</h5>
											</a>
										</div>
									@endif
								@endforeach
								@if($count == 0)
									<div class="blockquote">
										<h5>No hay declaraciones, <b>ayúdanos</b> a buscar información.</h5>
									</div>
								@endif
							@else
								<div class="blockquote">
									<h5>No hay declaraciones, <b>ayúdanos</b> a buscar información.</h5>
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-5">
			<h6 style="text-align: left">Comparte esta información para que todos voten informados.</h6>
			<!-- Buttons start here. Copy this ul to your document. -->
			<ul class="rrssb-buttons clearfix">
			<li class="rrssb-email">
			<!-- Replace subject with your message using URL Encoding: http://meyerweb.com/eric/tools/dencoder/ -->
			<a href="mailto:?Subject=Información sobre {{$politician->name}}&Body=Información es del candidato {{$politician->name}}  Fuente: {{URL::to('/')}}/politician/profile/{{str_slug($politician->name)}}">
			<span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28"><path d="M20.11 26.147c-2.335 1.05-4.36 1.4-7.124 1.4C6.524 27.548.84 22.916.84 15.284.84 7.343 6.602.45 15.4.45c6.854 0 11.8 4.7 11.8 11.252 0 5.684-3.193 9.265-7.398 9.3-1.83 0-3.153-.934-3.347-2.997h-.077c-1.208 1.986-2.96 2.997-5.023 2.997-2.532 0-4.36-1.868-4.36-5.062 0-4.75 3.503-9.07 9.11-9.07 1.713 0 3.7.4 4.6.972l-1.17 7.203c-.387 2.298-.115 3.3 1 3.4 1.674 0 3.774-2.102 3.774-6.58 0-5.06-3.27-8.994-9.304-8.994C9.05 2.87 3.83 7.545 3.83 14.97c0 6.5 4.2 10.2 10 10.202 1.987 0 4.09-.43 5.647-1.245l.634 2.22zM16.647 10.1c-.31-.078-.7-.155-1.207-.155-2.572 0-4.596 2.53-4.596 5.53 0 1.5.7 2.4 1.9 2.4 1.44 0 2.96-1.83 3.31-4.088l.592-3.72z"/></svg></span>
			<span class="rrssb-text">email</span>
			</a>
			</li>
			<li class="rrssb-facebook">
			<!--  Replace with your URL. For best results, make sure you page has the proper FB Open Graph tags in header: https://developers.facebook.com/docs/opengraph/howtos/maximizing-distribution-media-content/ -->
			<a href="https://www.facebook.com/sharer/sharer.php?u=http://historiapolitica.cl/politician/profile/{{str_slug($politician->name)}}" class="popup">
			<span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 29"><path d="M26.4 0H2.6C1.714 0 0 1.715 0 2.6v23.8c0 .884 1.715 2.6 2.6 2.6h12.393V17.988h-3.996v-3.98h3.997v-3.062c0-3.746 2.835-5.97 6.177-5.97 1.6 0 2.444.173 2.845.226v3.792H21.18c-1.817 0-2.156.9-2.156 2.168v2.847h5.045l-.66 3.978h-4.386V29H26.4c.884 0 2.6-1.716 2.6-2.6V2.6c0-.885-1.716-2.6-2.6-2.6z"/></svg></span>
			<span class="rrssb-text">facebook</span>
			</a>
			</li>
			<li class="rrssb-twitter">
			<!-- Replace href with your Meta and URL information  -->
			<a href="https://twitter.com/intent/tweet?text=Información sobre candidato {{$politician->name}} {{URL::to('/')}}/politician/profile/{{str_slug($politician->name)}} @histopolitica"
			class="popup">
			<span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28"><path d="M24.253 8.756C24.69 17.08 18.297 24.182 9.97 24.62a15.093 15.093 0 0 1-8.86-2.32c2.702.18 5.375-.648 7.507-2.32a5.417 5.417 0 0 1-4.49-3.64c.802.13 1.62.077 2.4-.154a5.416 5.416 0 0 1-4.412-5.11 5.43 5.43 0 0 0 2.168.387A5.416 5.416 0 0 1 2.89 4.498a15.09 15.09 0 0 0 10.913 5.573 5.185 5.185 0 0 1 3.434-6.48 5.18 5.18 0 0 1 5.546 1.682 9.076 9.076 0 0 0 3.33-1.317 5.038 5.038 0 0 1-2.4 2.942 9.068 9.068 0 0 0 3.02-.85 5.05 5.05 0 0 1-2.48 2.71z"/></svg></span>
			<span class="rrssb-text">twitter</span>
			</a>
			</li>
			</ul>
			<!-- Buttons end here -->
		</div>
	</div>
@endsection

