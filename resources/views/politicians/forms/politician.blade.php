 <div class="row">
	<div class="col-md-6">
		<div class="form-group">
			{!!Form::label('Nombre')!!}
			{!!Form::text('name',null,['class' => 'form-control required','placeholder' => 'Nombre Político'])!!}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			{!!Form::label('Ciudad')!!}
			 <?php $c = (!isset($politician))? "":$politician->city_id;  ?>
			{!!Form::select('city_id',$cities, $c ,['class' => 'form-control required','placeholder' => 'Seleccione Ciudad'])!!}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			{!!Form::label('Escuela/Colegio')!!}
			{!!Form::text('school',null,['class' => 'form-control required','placeholder' => 'Escuela o Colegió donde estudió'])!!}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			{!!Form::label('Partido Político')!!}
			{!!Form::text('politicalparty',null,['class' => 'form-control required','placeholder' => 'Partido Político'])!!}
		</div>

	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			
			{!!Form::label('Universisdad')!!}
			 <?php $u = (!isset($politician))? "":$politician->university_id;  ?>
			{!!Form::select('university_id',$universities, $u ,['class' => 'form-control required','placeholder' => 'Seleccione Universidad'])!!}
		</div>

	</div>
	<div class="col-md-6">
		<div class="form-group">
			{!!Form::label('Profesión')!!}
			{!!Form::text('profession',null,['class' => 'form-control required','placeholder' => 'Profesión'])!!}
		</div>
	</div>
</div>