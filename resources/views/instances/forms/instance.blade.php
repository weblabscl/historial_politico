<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			{!!Form::label('Título')!!}
			{!!Form::text('title',null,['class' => 'form-control required','placeholder' => 'Título'])!!}
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			{!!Form::label('Foto')!!}
			{!!Form::text('photo',null,['class' => 'form-control required','placeholder' => 'Url Foto en carpeta de casos'])!!}
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			{!!Form::label('Año del caso')!!}
			{!!Form::text('year',null,['class' => 'form-control required','placeholder' => 'Año de inicio del caso'])!!}
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			{!!Form::label('Descripción')!!}
			{!!Form::textarea('description',null,['class' => 'form-control required','placeholder' => 'Descripción'])!!}
		</div>
	</div>
</div>