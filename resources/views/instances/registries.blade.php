@extends('layouts.admin')
@section('title', 'Listado Registros')
@section('content')


<div class="container">
	<div class="row">
		<div class="col-md-12">
			
			<fieldset>
			<legend>Listado de Registros para "{{ $instance->title }}" {{ link_to_action('InstanceController@addregistry', $title = 'Crear Registro', $parameters = ["id" => $instance->id], $attributes = ['class' => 'btn btn-danger btn-sm','style' => '    float: right;'])  }}  </legend>
				<table class="table table-striped bg-info datatables">
					<thead>
						<tr>
							<th>Nombre</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($instance->registries as $registry) 
							<tr>
								<td>{{ $registry->title }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</fieldset>
		</div>
	</div>
</div>


@endsection

