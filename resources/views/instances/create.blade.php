@extends('layouts.public')
@section('title', 'Agregar Acontecimiento')
@section('content')


<fieldset style="text-align:left !important">

	
	<!-- Form Name -->
	<legend>Agregar Caso</legend>

	{!! Form::open(array('url' => '/instances', 'method' => 'post','id' => 'InstanceFormCreate')) !!}
	
		@include('instances.forms.instance')
	
		{!! Form::submit('Agregar',['class' => 'btn btn-primary']); !!}

	{!! Form::close() !!}

	</fieldset>

	<script>
		!function($) {
	    	$("#InstanceFormCreate").validate()
	    	$("#date").inputmask('')
		}(jQuery)
	</script>

@endsection

