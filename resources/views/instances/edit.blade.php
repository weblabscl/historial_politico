@extends('layouts.admin')
@section('title', 'Editar Acontecimiento')
@section('content')


<fieldset style="text-align:left !important">

	<!-- Form Name -->
	<legend>Editar Caso</legend>

	{!! Form::model($instance,['route' => ['instances.update', $instance->id],'method' => 'PUT','id' => 'InstanceFormEdit']) !!}
		@include('instances.forms.instance')
		{!!Form::submit('Editar caso',['class' => 'btn btn-primary'])!!}
	{!! Form::close()!!}

	</fieldset>

	<script>
		!function($) {
	    	$("#InstanceFormEdit").validate()
		}(jQuery)
	</script>

@endsection

