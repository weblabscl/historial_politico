@extends('layouts.admin')
@section('title', 'Listado Casos')
@section('content')


<div class="container">
	<div class="row">
		<div class="col-md-12">
			
			<fieldset>
				<legend>Listado de Casos</legend>
				<table class="table table-striped bg-info datatables">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Registros</th>
							<th>Opción</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($instances as $instance) 
							<tr>
								<td>{{ $instance->title }}</td>
								<td style="text-align:center"><p style="margin-top: 7px;  margin-bottom: 0px;"><span class="label label-warning">{{ count($instance->registries) }}</span></p></td>
								<td>
									{{ link_to_action('InstanceController@registries', $title = 'Registros', $parameters = ["id" => $instance->id], $attributes = ['class' => 'btn btn-primary btn-sm'])  }} 
									{{ link_to_action('InstanceController@edit', $title = 'Editar', $parameters = ["id" => $instance->id], $attributes = ['class' => 'btn btn-danger btn-sm'])  }} 
									@if($instance->state)
										{{ link_to_action('InstanceController@tobeapproved', $title = 'A Revisión', $parameters = ["id" => $instance->id], $attributes = ['class' => 'btn btn-warning btn-sm'])  }} 
									@else
										{{ link_to_action('InstanceController@approved', $title = 'Aprobar', $parameters = ["id" => $instance->id], $attributes = ['class' => 'btn btn-success btn-sm'])  }} 
									@endif

								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</fieldset>
		</div>
	</div>
</div>


@endsection

