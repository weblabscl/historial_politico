@extends('layouts.admin')
@section('title', 'Agregar candidato')
@section('content')


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<fieldset>
				<legend>Listado de Usuarios</legend>
				<table class="table">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Email</th>
							<th>Tipo</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $u)
							<tr>
								<td>{{$u->name}}</td>
								<td>{{$u->email}}</td>
								<td>
									<span class="label label-danger"><?= (!$u->type)? "Moderador":"Administrador";?></span>
								</td>
								<td>
									@if($u->type)
										<a class="btn btn-warning" href="/admin/be/<?= $u->id;?>/0">
											Solo Moderar
										</a>
									@else
										<a class="btn btn-success" href="/admin/be/<?= $u->id;?>/1">
											Administrador
										</a>
									@endif
								</td>
							</tr>	
						@endforeach
					</tbody>
				</table>
			</fieldset>
		</div>
	</div>
</div>

<script>
	!function($) {
    		
	}(jQuery)
</script>
@endsection