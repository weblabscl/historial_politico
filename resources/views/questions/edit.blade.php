@extends('layouts.admin')
@section('title', 'Editar candidato')
@section('content')


<fieldset style="text-align:left !important">

	<!-- Form Name -->
	<legend>Editar Item Encuesta</legend>

	{!! Form::model($question,['route' => ['question.update', $question->id],'method' => 'PUT','id' => 'QuestionFormEdit']) !!}
		@include('questions.forms.question')
		{!!Form::submit('Editar',['class' => 'btn btn-primary'])!!}
	{!! Form::close()!!}

	</fieldset>

	<script>
		!function($) {
	    		$("#QuestionFormEdit").validate()
		}(jQuery);
	</script>

@endsection

