@extends('layouts.admin')
@section('title', 'Agregar Item Encuesta')
@section('content')


<fieldset style="text-align:left !important">

	<!-- Form Name -->
	<legend>Agregar Item Encuesta</legend>

	{!! Form::open(array('url' => '/question', 'method' => 'post','id' => 'QuestionFormCreate')) !!}
		
		@include('questions.forms.question')
		{!! Form::submit('Agregar Item Encuesta'); !!}
		
	{!! Form::close() !!}

	</fieldset>

	<script>
		!function($) {
	    		$("#QuestionFormCreate").validate()
		}(jQuery);
	</script>

@endsection

