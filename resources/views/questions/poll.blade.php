@extends('layouts.public')
@section('title', 'Encuentra tu Candidato')
@section('content')

<div class="row titles">
	<div class="col-md-12 text-center">
		<h2 class="section-heading">Te ayudamos a encontrar tu candidato</h2>
		<hr class="primary">
		<h3><small>El resultado será en referencia a los planteamientos que han mostrado los candidatos y no refleja exactamente tu manera de pensar</small></h3>
	</div>
</div>

<div class="row">
	<div class="col-md-12" id="result">
		<h2><small>Tu candidato es <span class="text-primary" id="nombre"></span> con un <span class="text-primary"><span id="porcentaje"></span>%</span>. Respuestas : <span class="text-primary"><span id="respuestas"></span>/{{sizeof($questions)}}</span></small></h2>
		<hr class="primary">
		<div class="row" id="winner">
		</div>
		<!--div class="row">
			<div class="col-md-6">
				<div id="winner"></div>
			</div>
			<div class="col-md-6">
				<ul id="respuestas-lista" class="respuestas-lista"></ul>
			</div-->
	</div>
	<div class="col-md-12" id="results">
		<h2><small>Al parecer hay más de una opción. Favor escoge <span class="text-primary">Solo una</span></small></h2>
		<hr class="primary">
		<div class="row" id="winners">
		</div>
	</div>
</div>
<div class="row" id="load">
	<div class="col-md-12">
		<i class="fa fa-legal fa-spin text-primary fa-5x"></i>
		<h2><small>Buscando coincidencias...</small></h2>
	</div>
</div>

<div class="row" id="poll">
	<div class="col-md-12 text-center">
		<!-- Form Name -->
		<div class="container">
		    <div class="row">
		    	<section>
		        <div class="wizard">
		            <div class="wizard-inner">
		                <div class="connecting-line"></div>
		                <ul class="nav nav-tabs" role="tablist">
		                	@foreach ($questions as $question)
		                		@if($loop->index)
			                   		<li role="presentation" class="disabled">
			                   	@else
			                   		<li role="presentation" class="active">
			                   	@endif
			                        <a href="#step{{$loop->index}}" data-toggle="tab" aria-controls="step{{$loop->index}}" role="tab" title="{{$question->type}}">
			                            <span class="round-tab">
			                                <i class="{{$question->icon}}"></i>
			                            </span>
			                        </a>
			                    </li>
		                    @endforeach
		                </ul>
		            </div>
		            <form role="form" id="form1">
		                <div class="tab-content">
							@forelse ($questions as $question)
		                		@if($loop->index)
		                   			<div class="tab-pane" role="tabpanel" id="step{{$loop->index}}">
			                   	@else
		                   			<div class="tab-pane active" role="tabpanel" id="step{{$loop->index}}">
			                   	@endif

			                        <div class="{{$loop->index}}">
			                            <div class="row">
			                            	<div class="col-md-12 text-center question">
			                            		<h1>{{$question->description}}</h1>
			                            		<input type="hidden" name="question_{{$question->id}}" id="question_{{$question->id}}" value="" class="questions required">
			                            	</div>
			                            	<div class="col-md-12 text-center">
			                            		<div class="btn-group btn-group-lg" role="group" aria-label="Large button group"> 
			                            			<button type="button" class="btn next-step btn-default" value="2" data-question="{{$question->id}}">NO</button>
			                            			<button type="button" class="btn next-step btn-default" value="1" data-question="{{$question->id}}">SI</button>
			                            		</div>
			                            	</div>

			                            	@if($loop->last)
				                            	<div class="col-md-12 text-right terminar">
				                            		<button type="button" class="btn-primary btn btn-xl" id="finish">Terminar</button>
				                            	</div>
				                            @endif
			                            	<!--div class="col-md-6 text-center">
			                            		<button type="button" class="btn btn-secondary next-step">No</button>
			                            	</div>
			                            	<div class="col-md-6 text-center">
			                            		<button type="button" class="btn btn-secondary next-step">Si</button>
			                            	</div-->
			                            </div>
			                        </div>
			                    </div>
			                @empty
		                    	<h5>No hay Preguntas</h5>
		                    @endforelse
		                    <div class="clearfix"></div>
		                </div>
		            </form>
		        </div>
		    </section>
		   </div>
		</div>
	</div>
</div>

<div class="modal fade text-left" tabindex="-1" role="dialog" id="modalNodeAcuerdo">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Lamentamos que no coincidas con el resultado</h4>
			</div>
			<form action="" id="form2">
				<input type="hidden" name="id" id="insertPolitician">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="Contenido o Cita">Cuéntanos porque...</label>
								<textarea class="form-control required" placeholder="Cuéntanos tu opinión" name="observation" cols="50" rows="10"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">cancelar</button>
					<button class="btn btn-primary">Guardar</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

	<script>
		!function($) {

			compartirFb = function(e){
				elem = $(e)
				name = elem.data('name')
				image = elem.data('image')
				percent = elem.data('percent')

				postToFeed(name,image,percent);

				return false;
			}

			$("#form2").submit(function(e){
				
				if($(this).valid()){
					$.getJSON('question/editSurvey',$(this).serialize(),function(d){
						$("#modalNodeAcuerdo").modal('hide')
						$("#insertPolitician").val('')
						flash_success("Gracias por compartir tu opinión con nostros")
						up()
						
					})	
				}

				e.preventDefault()
				return false
			})

			noDeacuerdo = function(candidato,id){

				$("#modalNodeAcuerdo").modal('show')
				$("#insertPolitician").val(id)
			}

			escoge = function(candidato){
				console.log(candidato)
				$.getJSON('question/selectPolitician',{candidato:candidato},function(d){
					if(d == 1){
						flash_success("Gracias por compartir tu opinión con nostros")
						$(".panel-result").hide()
						$("#candidato_"+candidato).show()
						up()
					}else{
						flash_error("Lo sentimos ya tenemos registrada tu opción de candidato")
					}
				})		
			}

			$("form").validate(
				{
					ignore: "",
					errorPlacement: function(error, element) {} 
				}
			)

			$("#finish").click(function(){

				if(!$("#form1").valid()){
					flash_warning("Favor responder todas las preguntas")

				}else{

					up()
					$("#poll").hide()
					$("#load").show()
					$.getJSON('question/getPolitician',$("#form1").serialize(),function(d){

						$("#load").hide()

						if(d.Ganadores == 1){

							if(d.Candidatos){
								$.each(d.Candidatos,function(i,v){
									if(!i){
										$("#nombre").html(v.Candidato)
										$("#porcentaje").html(v.Porcentaje)
										$("#respuestas").html(v.Cantidad)

										var respuestas = '';
										if(v.Respuestas){
											$.each(v.Respuestas,function(i,v){

												var clase = v.Coincide?'si':'no'
												
												var icon = v.Coincide?'fa-check':'fa-close'

												respuestas = respuestas +'<li class="'+clase+'">'+'<i class="fa '+icon+'"></i>'+' '+v.Contenido+'</li>'

											})
										}

										var candidato = '<div class="panel panel-default panel-result">'+
    										'<div class="panel-body no-margin">'+
    											'<div class="row">'+
    												'<div class="col-md-3">'+
    													'<div class="">'+
															'<img src="'+root+'img/candidatos/'+v.Foto+'" alt="" class="photo_winners">'+
															'<div class="photo_name winners">'+
																'<span>'+v.Candidato+'</span>'+
															'</div>'+
														'</div>'+
    												'</div>'+
    												'<div class="col-md-6 padding-40">'+
    													'<ul class="respuestas-lista">'+
    														respuestas+
    													'</ul>'+
    													'<hr  class="primary">'+
														'<button class="btn btn-primary" data-image="'+root+'img/candidatos/'+v.Foto+'" data-name="'+v.Candidato+'" data-percent="'+v.Porcentaje+'" onclick="compartirFb(this)"><i class="fa fa-facebook-official"></i> Compartir</button>'+
														'<a class="twitter-share-button btn btn-primary"class="btn btn-primary" href="https://twitter.com/intent/tweet?hashtags=presidensiales2017&via=histopolitica&text=+'+encodeURI("Con un "+v.Porcentaje+"% de coincidencia mi candidato es: "+v.Candidato+". Descubre el tuyo en www.historiapolitica.cl")+'"><i class="fa fa-twitter"></i> Tweet</a>'+	
    												'</div>'+
   													'<div class="col-md-3 padding-40">'+
														'<i class="fa fa-bullhorn fa-4x"></i>'+
														'<hr  class="primary">'+
														'<button type="button" class="btn btn-xs" onclick="noDeacuerdo('+v.Id+','+v.insert+')"><i class="fa fa-close"></i> No concuerdo</button>'+
    												'</div>'+
    											'</div>'+
    										'</div>'+
  										'</div>'
						
										$("#winner").append(candidato)

										/*if(v.Respuestas){
											$.each(v.Respuestas,function(i,v){

												var clase = v.Coincide?'si':'no'
												
												var icon = v.Coincide?'fa-check':'fa-close'

												var respuesta = '<li class="'+clase+'">'+'<i class="fa '+icon+'"></i>'+' '+v.Contenido+'</li>'

												$("#respuestas-lista").append(respuesta)	
											})
										}*/
									}
									
								})
							}
							$("#result").show()
						}else{

							if(d.Candidatos){
								$.each(d.Candidatos,function(i,v){
									if(v.Ganador){
										$("#nombre").html(v.Candidato)
										$("#porcentaje").html(v.Porcentaje)
										$("#respuestas").html(v.Cantidad)

										var respuestas = '';
										if(v.Respuestas){
											$.each(v.Respuestas,function(i,v){

												var clase = v.Coincide?'si':'no'
												
												var icon = v.Coincide?'fa-check':'fa-close'

												respuestas = respuestas +'<li class="'+clase+'">'+'<i class="fa '+icon+'"></i>'+' '+v.Contenido+'</li>'

											})
										}

										var candidato = '<div class="panel panel-default panel-result" id="candidato_'+v.Id+'">'+
    										'<div class="panel-body no-margin">'+
    											'<div class="row">'+
    												'<div class="col-md-3">'+
    													'<div class="">'+
															'<img src="'+root+'img/candidatos/'+v.Foto+'" alt="" class="photo_winners">'+
															'<div class="photo_name winners">'+
																'<span>'+v.Candidato+'</span>'+
															'</div>'+
														'</div>'+
    												'</div>'+
    												'<div class="col-md-6 padding-40">'+
    													'<ul class="respuestas-lista">'+
    														respuestas+
    													'</ul>'+
    													'<hr  class="primary">'+
														'<button class="btn btn-primary" data-image="'+root+'img/candidatos/'+v.Foto+'" data-name="'+v.Candidato+'" data-percent="'+v.Porcentaje+'" onclick="compartirFb(this)"><i class="fa fa-facebook-official"></i> Compartir</button>'+
														'<a class="twitter-share-button btn btn-primary"class="btn btn-primary" href="https://twitter.com/intent/tweet?hashtags=presidensiales2017&via=histopolitica&text=+'+encodeURI("Con un "+v.Porcentaje+"% de coincidencia mi candidato es: "+v.Candidato+". Descubre el tuyo en www.historiapolitica.cl")+'"><i class="fa fa-twitter"></i> Tweet</a>'+
    												'</div>'+
   													'<div class="col-md-3 padding-40">'+
														'<i class="fa fa-bullhorn fa-4x"></i>'+
														'<hr  class="primary">'+
														'<button type="button" class="btn btn-primary" onclick="escoge('+v.Id+')">Escoger <i class="glyphicon glyphicon-chevron-right"></i></button>'+
    												'</div>'+
    											'</div>'+
    										'</div>'+
  										'</div>'

										$("#winners").append(candidato)
									}
									
								})
							}

							$("#results").show()
						}

					})			
				}
			})
			
		    //Initialize tooltips
		    $('.nav-tabs > li a[title]').tooltip();
		    
		    //Wizard
		    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

		        var $target = $(e.target);
		    
		        if ($target.parent().hasClass('disabled')) {
		            return false;
		        }
		    });

		    $(".next-step").click(function (e) {

		    	var btn = $(this)
				
				var id = btn.attr('data-question')

				if(btn.hasClass('active')){
					btn.removeClass('active btn-primary')
					$("#question_"+id).val('')
				}else{
					$("#question_"+id).val(btn.val())
					var $active = $('.wizard .nav-tabs li.active');
			        $active.next().removeClass('disabled');
			        nextTab($active);

					btn.addClass('active btn-primary')
					if(btn.val() == 1){
						btn.prev().removeClass('active btn-primary');
					}else{
						btn.next().removeClass('active btn-primary');
					}
				}	

		    });

		    $(".prev-step").click(function (e) {

		        var $active = $('.wizard .nav-tabs li.active');
		        prevTab($active);

		    });

			nextTab = function(elem){
				$(elem).next().find('a[data-toggle="tab"]').click();
			}
			
			prevTab = function(elem) {
			    $(elem).prev().find('a[data-toggle="tab"]').click();
			}


			//according menu
		
	    	//Add Inactive Class To All Accordion Headers
		    $('.accordion-header').toggleClass('inactive-header');
			
			//Set The Accordion Content Width
			var contentwidth = $('.accordion-header').width();
			$('.accordion-content').css({});
			
			//Open The First Accordion Section When Page Loads
			$('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
			$('.accordion-content').first().slideDown().toggleClass('open-content');
			
			// The Accordion Effect
			$('.accordion-header').click(function () {
				if($(this).is('.inactive-header')) {
					$('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
					$(this).toggleClass('active-header').toggleClass('inactive-header');
					$(this).next().slideToggle().toggleClass('open-content');
				}

				else {
					$(this).toggleClass('active-header').toggleClass('inactive-header');
					$(this).next().slideToggle().toggleClass('open-content');
				}
			
			});
			
		}(jQuery);
	</script>

	<style>
	#results,#result,#load{
		display: none;
	}
</style>
@endsection

