 <div class="row">
	<div class="col-md-6">
		<div class="form-group">
			{!!Form::label('Item Encuesta')!!}
			{!!Form::text('description',null,['class' => 'form-control required','placeholder' => 'Escriba una item para la encuesta'])!!}
		</div>

		<div class="form-group">
			{!!Form::label('Área de Item')!!}
			{!!Form::text('type',null,['class' => 'form-control required','placeholder' => 'Escriba en que área se desenvuelve el item'])!!}
		</div>
	
		<div class="form-group">
			{!!Form::label('Icono del item')!!}
			{!!Form::text('icon',null,['class' => 'form-control required','placeholder' => 'Icono del item'])!!}
		</div>

	</div>
</div>