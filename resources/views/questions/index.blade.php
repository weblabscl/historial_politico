@extends('layouts.admin')
@section('title', 'Agregar candidato')
@section('content')


<div class="container">
	<div class="row">
		<div class="col-md-12">
			
			<fieldset>
				<legend>Listado de  Items Encuesta {{ link_to_action('QuestionController@create', $title = 'Crear Item', $parameters = [], $attributes = ['class' => 'btn btn-danger btn-sm','style' => '    float: right;'])  }}  </legend>
				<table class="table table-striped bg-info datatables">
					<thead>
						<tr>
							<th>Descripción</th>
							<th>Cantidad de Respuestas/Políticos</th>
							<th>Opción</th>
						</tr>
					</thead>
					<tbody>
							@foreach ($questions as $question) 
								<tr>
									<td>{{ $question->description }}</td>
									<td style="text-align:center"><p style="    margin-top: 7px;  margin-bottom: 0px;"><span class="label label-warning">{{ count($question->politicians) }}</span></p></td>
									<td>
										{{ link_to_action('QuestionController@edit', $title = 'Editar', $parameters = ["id" => $question->id], $attributes = ['class' => 'btn btn-danger btn-sm'])  }}  
									</td>
								</tr>
							@endforeach
					</tbody>
				</table>
			</fieldset>
		</div>
	</div>
</div>
@endsection