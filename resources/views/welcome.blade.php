@extends('layouts.master')
@section('title', 'Home')
@section('content')
 <!-- Navigation -->
        <nav class="navbar fixed-top navbar-toggleable-md navbar-light" id="mainNav">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <a class="navbar-brand" href="#page-top">Historia Politica</a>
                <div class="collapse navbar-collapse" id="navbarExample">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#about">¿Qué es?</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#candidatos">Candidatos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#encuesta">Encuesta</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contact">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <header class="masthead">
            <div class="header-content">
                <div class="header-content-inner">
                    <h1 id="homeHeading">Nunca olvidemos lo que dicen los politicos</h1>
                    <hr>
                    <p>Historial informativo de candidatos o politicos en chile.</p>
                    <a class="btn btn-primary btn-xl" href="#about">Seguir leyendo</a>
                </div>
            </div>
        </header>

        <section class="bg-primary" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2 text-center">
                        <h2 class="section-heading text-white">Todo lo que necesitas saber de los candidatos</h2>
                        <hr class="light">
                        <p class="text-faded">Muchos tenemos una memoria a corto plazo y se nos olvida información que puede ser de utilidad antes de confiar en un próximo candidato, acá te entregamos información sobre los candidatos y algunas citas o declaraciones realizadas en el trancurso de los años.</p>
                        <a class="btn btn-default btn-xl sr-button" href="#services">Refresca tu memoría ahora!</a>
                    </div>
                </div>
            </div>
        </section>

        <section id="candidatos">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">Candidatos Presidenciales</h2>
                        <hr class="primary">
                        @yield('content')
                    </div>
                </div>
            </div>
            <!-- <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-diamond text-primary sr-icons"></i>
                            <h3>Sturdy Templates</h3>
                            <p class="text-muted">Our templates are updated regularly so they don't break.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-paper-plane text-primary sr-icons"></i>
                            <h3>Ready to Ship</h3>
                            <p class="text-muted">You can use this theme as is, or you can make changes!</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i>
                            <h3>Up to Date</h3>
                            <p class="text-muted">We update dependencies to keep things fresh.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-heart text-primary sr-icons"></i>
                            <h3>Made with Love</h3>
                            <p class="text-muted">You have to make your websites with love these days!</p>
                        </div>
                    </div>
                </div>
            </div> -->
        </section>

        <section class="no-padding" id="portfolio">
            <div class="container-fluid">
                <!-- <div class="row no-gutter popup-gallery">
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="img/portfolio/fullsize/1.jpg">
                            <img class="img-fluid" src="img/portfolio/thumbnails/1.jpg" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="img/portfolio/fullsize/2.jpg">
                            <img class="img-fluid" src="img/portfolio/thumbnails/2.jpg" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="img/portfolio/fullsize/3.jpg">
                            <img class="img-fluid" src="img/portfolio/thumbnails/3.jpg" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="img/portfolio/fullsize/4.jpg">
                            <img class="img-fluid" src="img/portfolio/thumbnails/4.jpg" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="img/portfolio/fullsize/5.jpg">
                            <img class="img-fluid" src="img/portfolio/thumbnails/5.jpg" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="img/portfolio/fullsize/6.jpg">
                            <img class="img-fluid" src="img/portfolio/thumbnails/6.jpg" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div> -->
            </div>
        </section>

        <div class="call-to-action bg-dark" id="encuesta">
            <div class="container text-center">
                <h2>Encuesta presidencial <small>Vota por un candidato en nuestra encuesta anonima.</small></h2>
                <a class="btn btn-default btn-xl sr-button" href="">Votar Ahora!</a>
            </div>
        </div>

        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2 text-center">
                        <h2 class="section-heading">¿Quieres ayudar a historiapolitica.cl?</h2>
                        <hr class="primary">
                        <p>Si quieres ayudarnos con ideas o con ser parte de este proyecto envíanos un correo.</p>
                    </div>
                    <!-- <div class="col-lg-4 offset-lg-2 text-center">
                        <i class="fa fa-phone fa-3x sr-contact"></i>
                        <p>123-456-6789</p>
                    </div> -->
                    <div class="col-lg-4 offset-lg-4 text-center">
                        <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                        <p><a href="mailto:info@historiapolitica.cl">info@historiapolitica.cl</a></p>
                    </div>
                </div>
            </div>
        </section>
@endsection()