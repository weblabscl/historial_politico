<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				{!!Form::label('Candidato Involucrado')!!}
				 <?php $c = (!isset($event))? "":$event->politician_id;  ?>
				{!!Form::select('politician_id',$politicians, $c ,['class' => 'form-control required','placeholder' => 'Seleccione Candidato'])!!}
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<!-- <label class="control-label" for="name">Fecha en que ocurrió</label>  
				<input id="date" name="date" class="form-control required" data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy"> -->
				{!!Form::label('Fecha en que ocurrió')!!}
				 <?php $date = (!isset($event))? "":Carbon\Carbon::parse($event->date)->format('d/m/Y');  ?>
				{!!Form::text('date',$date,['id' => 'date','data-inputmask' => "'alias': 'date'",'class' => 'form-control required','placeholder' => 'dd/mm/yyyy'])!!}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				{!!Form::label('Contenido o Cita')!!}
				{!!Form::textarea('description',null,['class' => 'form-control required','placeholder' => 'Contenido o Cita'])!!}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<div class="checkbox">
					<label>
					 <?php 
					 	$checked = false;
					 	if(isset($event)){
					 		if($event->quote == 1){
					 			$checked = true;
					 		}
					 	}
					  ?>
					{!!Form::checkbox('quote',1,$checked)!!} Marca acá si el contenido del mensaje fue una declaración de la persona.
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<div class="form-group">
					{!!Form::label('Fuente de la información')!!}
					{!!Form::text('link',null,['class' => 'form-control required url','placeholder' => 'http://....'])!!}
				</div>
			</div>
		</div>
	</div>