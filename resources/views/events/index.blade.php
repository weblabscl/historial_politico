@extends('layouts.admin')
@section('title', 'Agregar candidato')
@section('content')


<div class="container">
	<div class="row">
		<div class="col-md-12">
			
			<fieldset>
				<legend>Listado de Acontecimientos/Citas {{ link_to_action('EventController@create', $title = 'Crear Acontecimiento', $parameters = [], $attributes = ['class' => 'btn btn-danger btn-sm','style' => '    float: right;'])  }}  </legend>
				<div>
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#aprobar" aria-controls="aprobar" role="tab" data-toggle="tab">Por Aprobar</a></li>
						<li role="presentation"><a href="#aceptados" aria-controls="aceptados" role="tab" data-toggle="tab">Publicados</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="aprobar">
							<div class="row">
								<div class="col-md-12 padding-40">
									<table class="table table-striped bg-info datatables">
										<thead>
											<tr>
												<th>Descripción</th>
												<th>Político</th>
												<th>Link</th>
												<th>Cita</th>
												<th width="100px">Fecha</th>
												<th width="350px">Opción</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($events_pendient as $event) 
												<tr>
													<td>{{ str_limit($event->description, $limit = 100, $end = '...') }} </td>
													<td>{{ $event->politician->name }}</td>
													<td><a href="{{ $event->link }}"  target="_blank"><i class="fa fa-search"></i></a></td>
													<td>
														@if( $event->quote  == 1)
															<p style="    margin-top: 0px;  margin-bottom: 0px;"><span class="label label-danger">Si</span></p>
														@else
															<p style="    margin-top: 0px;  margin-bottom: 0px;"><span class="label label-danger">No</span></p>
														@endif
													</td>
													<td>{{ Carbon\Carbon::parse($event->date)->format('d/m/Y') }}</td>
													<td>
														<span id="event-{{$event->id}}" data-nombre="{{$event->politician->name}}" data-detalle="{{$event->description}}" data-fecha="{{$event->date}}"></span>
														<a onclick="verDetalle('{{$event->id}}')" class="btn btn-sm btn-warning">Ver Detalle</a>
														{{ link_to_action('EventController@edit', $title = 'Editar', $parameters = ["id" => $event->id], $attributes = ['class' => 'btn btn-warning btn-sm'])  }}  
														{{ link_to_action('EventController@approved', $title = 'Aprobar', $parameters = ["id" => $event->id], $attributes = ['class' => 'btn btn-success btn-sm'])  }} 
														{{ Form::open(['style'=>'float:right', 'route' => ['event.destroy',$event->id],'method' => 'DELETE']) }}
															{{ Form::submit('Borrar',['class' => 'btn btn-sm btn-danger']) }}
														{{ Form::close() }} 
														<!--{{ link_to_action('EventController@destroy', $title = 'Eliminar', $parameters = ["id" => $event->id], $attributes = ['class' => 'btn btn-danger btn-sm'])  }}  -->
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="aceptados">
							<div class="row">
								<div class="padding-40 col-md-12">
									<table class="table table-striped bg-info datatables">
										<thead>
											<tr>
												<th>Descripción</th>
												<th>Político</th>
												<th>Link</th>
												<th>Cita</th>
												<th width="100px">Fecha</th>
												<th width="300px">Opción</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($events as $event) 
												<tr>
													<td>{{ str_limit($event->description, $limit = 100, $end = '...') }} </td>
													<td>{{ $event->politician->name }}</td>
													<td><a href="{{ $event->link }}" ><i class="fa fa-search"></i></a></td>
													<td>
														@if( $event->quote  == 1)
															<p style="    margin-top: 0px;  margin-bottom: 0px;"><span class="label label-danger">Si</span></p>
														@else
															<p style="    margin-top: 0px;  margin-bottom: 0px;"><span class="label label-danger">No</span></p>
														@endif
													</td>
													<td>{{ Carbon\Carbon::parse($event->date)->format('d/m/Y') }}</td>
													<td>
														<span id="event-{{$event->id}}" data-nombre="{{$event->politician->name}}" data-detalle="{{$event->description}}" data-fecha="{{$event->date}}"></span>
														<a onclick="verDetalle('{{$event->id}}')" class="btn btn-sm btn-warning">Ver Detalle</a>
														{{ link_to_action('EventController@edit', $title = 'Editar', $parameters = ["id" => $event->id], $attributes = ['class' => 'btn btn-danger btn-sm'])  }}  
														{{ link_to_action('EventController@tobeapproved', $title = 'A Revisión', $parameters = ["id" => $event->id], $attributes = ['class' => 'btn btn-success btn-sm'])  }}  
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
				
			</fieldset>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalDetalle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="labelModal"></h4>
			</div>
			<div class="modal-body">
				<p id="modal-body-content"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<script>
	!function($) {
    		$("#EventFormCreate").validate()
    		$("#date").inputmask('')
    		verDetalle = function(id){
    			$("#modal-body-content").html($("#event-"+id).attr("data-detalle"))
    			$("#labelModal").html($("#event-"+id).attr("data-nombre")+" | "+$("#event-"+id).attr("data-fecha"))
    			$("#modalDetalle").modal("show")
    		}
	}(jQuery)
</script>
@endsection