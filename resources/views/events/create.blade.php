@extends('layouts.public')
@section('title', 'Agregar Acontecimiento')
@section('content')


<fieldset style="text-align:left !important">

	
	<!-- Form Name -->
	<legend>Agregar Acontecimiento o Cita</legend>

	{!! Form::open(array('url' => '/event', 'method' => 'post','id' => 'EventFormCreate')) !!}
	
		@include('events.forms.event')
	
		{!! Form::submit('Colaborar',['class' => 'btn btn-primary']); !!}

	{!! Form::close() !!}

	</fieldset>

	<script>
		!function($) {
	    	$("#EventFormCreate").validate()
	    	$("#date").inputmask('')
		}(jQuery)
	</script>

@endsection

