@extends('layouts.admin')
@section('title', 'Editar Acontecimiento')
@section('content')


<fieldset style="text-align:left !important">

	<!-- Form Name -->
	<legend>Editar Acontecimiento</legend>

	{!! Form::model($event,['route' => ['event.update', $event->id],'method' => 'PUT','id' => 'EventFormEdit']) !!}
		@include('events.forms.event')
		{!!Form::submit('Editar acontecimiento',['class' => 'btn btn-primary'])!!}
	{!! Form::close()!!}

	</fieldset>

	<script>
		!function($) {
	    	$("#EventFormEdit").validate()
	    	$("#date").inputmask('')
		}(jQuery)
	</script>

@endsection

