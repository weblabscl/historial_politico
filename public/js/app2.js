 
/* Functiones del sistema*/
function up(){
 	$("html, body").animate({ scrollTop: 0 }, "slow")
	return null;
}


Messenger.options = {
    extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
    theme: 'air'
}

function flash_success(message){
	Messenger().post(message)
}

function flash_error(message){
	Messenger().post({
		message: message,
		type: 'error',
		showCloseButton: true
	})
}

function flash_warning(message){
	Messenger().post({
		message: message,
		type: 'info',
		showCloseButton: true
	})
}